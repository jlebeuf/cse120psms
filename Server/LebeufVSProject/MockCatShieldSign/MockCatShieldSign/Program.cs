﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace MockCatShieldSign
{
    class Program
    {
        const int READ_BUFFER_SIZE = 128;
        static readonly byte[] serverConnectMessage = new byte[8] {0x01, 0x01, 0x23, 0x45, 0x67, 0x02, 0x10, 0x00};
        static readonly byte[] serialNumber = new byte[4];
        const int serverConnectMessageLength = 8;
        static readonly byte[] readBuffer = new byte[READ_BUFFER_SIZE];
        static int readBufferCount = 0;
        static int readBufferExpectedBytes = 1;
        const int port = 26262;

        static TcpClient client = null;
        static NetworkStream stream = null;

        static void Main(string[] args)
        {
            // MOCK SIGN TESTING ADDITION
            int randSerial = new Random().Next();
            Console.WriteLine("Mock sign, using random serial number: " + randSerial);
            serialNumber[0] = (byte)(randSerial >> 24);
            serialNumber[1] = (byte)(randSerial >> 16);
            serialNumber[2] = (byte)(randSerial >> 8);
            serialNumber[3] = (byte)(randSerial >> 0);
            serverConnectMessage[1] = serialNumber[0];
            serverConnectMessage[2] = serialNumber[1];
            serverConnectMessage[3] = serialNumber[2];
            serverConnectMessage[4] = serialNumber[3];


            // SETUP
            connectToServer();

            // LOOP
            while (true)
            {
                if (client != null && !client.Connected)
                    connectToServer();
                // MOCK SIGN TESTING ADDITION
                if (client.Client.Poll(0, SelectMode.SelectRead))
                {
                    byte[] b = new byte[1];
                    try {
                        if (client.Client.Receive(b, SocketFlags.Peek) == 0)
                            connectToServer();
                    } catch (Exception e) {
                        connectToServer();
                    }
                }
                // END MOCK SIGN TESTING ADDITION

                while (client.Available > 0)
                {
                    if (readBufferCount >= READ_BUFFER_SIZE)
                    {
                        Console.WriteLine("[Read buffer overflow!]");
                    } else
                    {
                        byte c = (byte)stream.ReadByte();
                        readBuffer[readBufferCount++] = c;
                    }
                    processReadBuffer();
                }
            }
        }

        static void connectToServer()
        {
            Console.WriteLine("[Connecting to server]");
            if (client != null)
                client.Client.Shutdown(SocketShutdown.Both);

            while (true)
            {
                try
                {
                    client = new TcpClient("localhost", port);
                    break;
                } catch (Exception e)
                {
                    continue;
                }
            }
            stream = client.GetStream();
            Console.WriteLine("[Connected]");

            for (int i = 0; i < serverConnectMessageLength; i++)
                stream.WriteByte(serverConnectMessage[i]);
            readBufferCount = 0;
        }

        static void clearScreen()
        {
            Console.WriteLine("*CLEAR SCREEN*");
            Console.WriteLine("==============");
        }

        static void processReadBuffer()
        {
            if (readBufferCount < 2)
                return;

            if (readBuffer[0] == 0x01)
            {
                clearScreen();
                for (int i = 0; i < readBufferCount - 2; i++)
                    readBuffer[i] = readBuffer[i+2];
                readBufferCount -= 2;
            }
            else
            if (readBuffer[0] == 0x07)
            {
                if ((readBufferCount - 3 - readBuffer[1]) >= 0)
                {
                    clearScreen();
                    for (int i = 2; i < readBuffer[1]+2; i++)
                    {
                        if (readBuffer[i] == 0x00)
                            Console.WriteLine();
                        else
                            Console.Write((char)readBuffer[i]);
                    }
                    Console.WriteLine();
                    int commandSize = 3 + readBuffer[1];
                    for (int i = 0; i < readBufferCount-commandSize; i++)
                        readBuffer[i] = readBuffer[i + commandSize];
                    readBufferCount -= commandSize;
                }
            }
            else
            if (readBuffer[0] == 0x11)
            {
                for (int i = 0; i < serverConnectMessageLength; i++)
                    stream.WriteByte(serverConnectMessage[i]);
                for (int i = 0; i < readBufferCount - 2; i++)
                    readBuffer[i] = readBuffer[i + 2];
                readBufferCount -= 2;
            }
        }


    }
}