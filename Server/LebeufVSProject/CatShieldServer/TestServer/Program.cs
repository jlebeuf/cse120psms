﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;

using CatShieldServer;

namespace TestServer
{
    class Program
    {
        static Server server = null;
        static IUserInterface userInterface = null;
        static UserData userData = null;
        static int sessionID;


        static void Main(string[] args)
        {
            server = new Server(26262, 2500);
            userInterface = server.GetUserInterface();
            sessionID = userInterface.NULL_SESSION_ID;

            string command = "";
            do
            {
                Console.WriteLine();
                Console.WriteLine();

                if (userInterface.LoggedIn(sessionID))
                {
                    Console.WriteLine("Logged in, session ID: " + sessionID);
                }
                else
                {
                    Console.WriteLine("Logged out");
                    sessionID = userInterface.NULL_SESSION_ID;
                    userData = null;
                }

                Console.WriteLine("Main Menu:");
                Console.WriteLine("[0] Quit");
                Console.WriteLine("[1] Login Menu");
                Console.WriteLine("[2] Accounts Menu");
                Console.WriteLine("[3] Broadcast Menu");
                Console.WriteLine("[4] Sign Menu");
                Console.Write("Command: ");
                command = Console.ReadLine();

                if (command == "0")
                {
                    break;
                }
                else if (command == "1")
                {
                    LoginMenu();
                }
                else if (command == "2")
                {
                    AccountsMenu();
                }
                else if (command == "3")
                {
                    BroadcastMenu();
                }
                else if (command == "4")
                {
                    SignMenu();
                }

            } while (command != "0");

            Console.WriteLine("Shutting down. Press any key to continue.");
            Console.ReadKey();
        }

        static void LoginMenu()
        {
            string command;
            do
            {
                Console.WriteLine("\nLogin Menu:");
                Console.WriteLine("[0] Cancel");
                Console.WriteLine("[1] Login");
                Console.WriteLine("[2] Logout");
                Console.Write("Command: ");
                command = Console.ReadLine();
                if (command == "0")
                {
                    break;
                }
                else if (command == "1")
                {
                    string username, password;
                    Console.WriteLine("Username [leave blank to cancel]: ");
                    username = Console.ReadLine();
                    if (username == "")
                        return;
                    Console.WriteLine("Password: ");
                    password = Console.ReadLine();

                    int sid = userInterface.Login(username, password);
                    userData = userInterface.GetUserData(sid);
                    if (sid == userInterface.NULL_SESSION_ID)
                        Console.WriteLine("Login failed");
                    else
                    {
                        Console.WriteLine("Login successful, sessionID: " + sid);
                        sessionID = sid;
                        return;
                    }
                }
                else if (command == "2")
                {
                    bool status = userInterface.Logout(sessionID);
                    if (status)
                    {
                        Console.WriteLine("Logout successful");
                        sessionID = userInterface.NULL_SESSION_ID;
                        userData = null;
                    }
                    else
                        Console.WriteLine("Logout failed");
                    return;
                }

            } while (command != "0");
        }

        static void AccountsMenu()
        {
            string command;
            do
            {
                Console.WriteLine("\nAccounts Menu:");
                Console.WriteLine("[0] Cancel");
                Console.WriteLine("[1] Create Account");
                Console.Write("Command: ");
                command = Console.ReadLine();
                if (command == "0")
                {
                    break;
                }
                else if (command == "1")
                {
                    string username, password, firstname, lastname, admin, canApproveReject;
                    Console.WriteLine("Username [leave blank to cancel]: ");
                    username = Console.ReadLine();
                    if (username == "")
                        return;
                    Console.WriteLine("Password: ");
                    password = Console.ReadLine();
                    if (password == "")
                        return;
                    Console.WriteLine("First name: ");
                    firstname = Console.ReadLine();
                    if (firstname == "")
                        return;
                    Console.WriteLine("Last name: ");
                    lastname = Console.ReadLine();
                    if (lastname == "")
                        return;
                    Console.WriteLine("Is admin [yes/no]: ");
                    admin = Console.ReadLine();
                    if (admin == "")
                        return;
                    Console.WriteLine("Can approve/reject broadcasts [yes/no]: ");
                    canApproveReject = Console.ReadLine();
                    if (canApproveReject == "")
                        return;

                    bool isAdmin = false;
                    if (admin.ToLower() == "yes")
                        isAdmin = true;
                    bool canAppRej = false;
                    if (canApproveReject.ToLower() == "yes")
                        canAppRej = true;

                    bool status = userInterface.CreateUser(sessionID, username, password, firstname, lastname, isAdmin, canAppRej);
                    if (status)
                        Console.WriteLine("Account created");
                    else
                        Console.WriteLine("Account creation failed");
                }

            } while (command != "0");
        }

        static void BroadcastMenu()
        {
            string command;
            do
            {
                Console.WriteLine("\nBroadcast Menu:");
                Console.WriteLine("[0] Cancel");
                Console.WriteLine("[1] View Pending Queue");
                Console.WriteLine("[2] View Approved");
                Console.WriteLine("[3] View Rejected");
                Console.WriteLine("[4] Approve Broadcast");
                Console.WriteLine("[5] Reject Broadcast");
                Console.WriteLine("[6] View Gateways");
                Console.WriteLine("[7] Make Broadcast");
                Console.Write("Command: ");
                command = Console.ReadLine();
                if (command == "0")
                {
                    break;
                }
                else if (command == "1")
                {
                    Broadcast[] broadcasts = userInterface.GetPendingBroadcasts();
                    Console.WriteLine("Pending broadcasts: " + broadcasts.Length);
                    for (int i = 0; i < broadcasts.Length; i++)
                    {
                        Console.WriteLine(broadcasts[i].ToString());
                    }
                }
                else if (command == "2")
                {
                    Broadcast[] broadcasts = userInterface.GetApprovedBroadcasts();
                    Console.WriteLine("Approved broadcasts: " + broadcasts.Length);
                    for (int i = 0; i < broadcasts.Length; i++)
                    {
                        Console.WriteLine(broadcasts[i].ToString());
                    }
                }
                else if (command == "3")
                {
                    Broadcast[] broadcasts = userInterface.GetRejectedBroadcasts();
                    Console.WriteLine("Rejected broadcasts: " + broadcasts.Length);
                    for (int i = 0; i < broadcasts.Length; i++)
                    {
                        Console.WriteLine(broadcasts[i].ToString());
                    }
                }
                else if (command == "4")
                {
                    UserData userData = userInterface.GetUserData(sessionID);
                    if (userData == null)
                    {
                        Console.WriteLine("Not logged in");
                        continue;
                    }
                    if (!userData.CanApproveAndRejectBroadcasts)
                    {
                        Console.WriteLine("Insufficient account permissions");
                        continue;
                    }
                    Console.WriteLine("Approve broadcast ID:");
                    string line = Console.ReadLine();
                    int broadcastID;
                    if (!Int32.TryParse(line, out broadcastID))
                    {
                        Console.WriteLine("Invalid number");
                        continue;
                    }
                    if (userInterface.ApproveBroadcast(sessionID, broadcastID))
                        Console.WriteLine("Broadcast approved");
                    else
                        Console.WriteLine("Failed to approve broadcast");
                }
                else if (command == "5")
                {
                    UserData userData = userInterface.GetUserData(sessionID);
                    if (userData == null)
                    {
                        Console.WriteLine("Not logged in");
                        continue;
                    }
                    if (!userData.CanApproveAndRejectBroadcasts)
                    {
                        Console.WriteLine("Insufficient account permissions");
                        continue;
                    }
                    Console.WriteLine("Reject broadcast ID:");
                    string line = Console.ReadLine();
                    int broadcastID;
                    if (!Int32.TryParse(line, out broadcastID))
                    {
                        Console.WriteLine("Invalid number");
                        continue;
                    }
                    if (userInterface.RejectBroadcast(sessionID, broadcastID))
                        Console.WriteLine("Broadcast rejected");
                    else
                        Console.WriteLine("Failed to reject broadcast");
                }
                else if (command == "6")
                {
                    string[] gateways = userInterface.GetGateways();
                    Console.WriteLine("Gateways: " + gateways.Length);
                    for (int i = 0; i < gateways.Length; i++)
                    {
                        Console.WriteLine(gateways[i]);
                    }
                }
                else if (command == "7")
                {
                    string message, gateway;
                    string[] gateways = new string[0];


                    Console.WriteLine("Message:");
                    message = Console.ReadLine();

                    Console.WriteLine("Gateways:");
                    do
                    {
                        gateway = Console.ReadLine();
                        if (gateway != "")
                        {
                            string[] newGateways = new string[gateways.Length+1];
                            for (int i = 0; i < gateways.Length; i++)
                                newGateways[i] = gateways[i];
                            newGateways[gateways.Length] = gateway;
                            gateways = newGateways;
                        }
                    } while (gateway != "");

                    if (userInterface.QueueBroadcast(sessionID, gateways, message))
                    {
                        Console.WriteLine("Broadcast queued");
                        return;
                    }
                    else
                    {
                        Console.WriteLine("Broadcast queuing failed");
                    }
                }
            } while (command != "0");
        }

        static void SignMenu()
        {
            string command;
            do
            {
                Console.WriteLine("\nSign Menu:");
                Console.WriteLine("[0] Cancel");
                Console.WriteLine("[1] View Connected Signs");
                Console.WriteLine("[2] Clear Sign Screen");
                Console.WriteLine("[3] Send Sign Message");
                Console.Write("Command: ");
                command = Console.ReadLine();
                if (command == "0")
                {
                    break;
                }
                else if (command == "1")
                {
                    Sign[] signs = userInterface.GetSigns();
                    Console.WriteLine("Connected signs: " + signs.Length);
                    for (int i = 0; i < signs.Length; i++)
                    {
                        Console.WriteLine(signs[i].ToString());
                    }
                }
                else if (command == "2")
                {
                    UserData userData = userInterface.GetUserData(sessionID);
                    if (userData == null)
                    {
                        Console.WriteLine("Not logged in");
                        continue;
                    }
                    Console.WriteLine("Sign ID:");
                    string line = Console.ReadLine();
                    int signID;
                    if (!Int32.TryParse(line, out signID))
                    {
                        Console.WriteLine("Invalid number");
                        continue;
                    }
                    if (userInterface.ClearSignScreen(sessionID, signID))
                        Console.WriteLine("Clear screen command sent");
                    else
                        Console.WriteLine("Failed to send clear screen command");
                }
                else if (command == "3")
                {
                    UserData userData = userInterface.GetUserData(sessionID);
                    if (userData == null)
                    {
                        Console.WriteLine("Not logged in");
                        continue;
                    }
                    Console.WriteLine("Sign ID:");
                    string line = Console.ReadLine();
                    int signID;
                    if (!Int32.TryParse(line, out signID))
                    {
                        Console.WriteLine("Invalid number");
                        continue;
                    }
                    Console.WriteLine("Message");
                    string message = Console.ReadLine();

                    if (userInterface.SendSignMessage(sessionID, signID, message))
                        Console.WriteLine("Sign message sent");
                    else
                        Console.WriteLine("Failed to send sign message");
                }
            } while (command != "0");
        }
    }
}
