﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldDevQuickTests
{
    class DatabaseHandlerTest : Test
    {
        class User
        {
            public string username;
            public string password;
            public string firstname;
            public string lastname;
            public User(string un, string pw, string fn, string ln) { username = un; password = pw; firstname = fn; lastname = ln; }
        }


        public DatabaseHandlerTest() : base("DatabaseHandlerTest") { }

        override public bool Run()
        {
            bool status = true;
            CatShieldServer.DatabaseHandler databaseHandler = new CatShieldServer.DatabaseHandler();

            User[] users = new User[10];
            for (int i = 1; i <= users.Length; i++)
                users[i-1] = new User("user"+i, "pass"+i, "first"+i, "last"+i);
            CatShieldServer.UserData userData;
            bool tempStatus;

            userData = databaseHandler.GetUserData("username");
            if (userData == null)
                Console.WriteLine("Failed to get 'username' user data");
            else
            {
                Console.WriteLine("Got user data for 'username':");
                printUserData(userData);
            }

            Console.WriteLine();
            Console.WriteLine("Creating users:");
            Console.WriteLine();

            for (int i = 0; i < users.Length; i++)
            {
                Console.WriteLine("Creating user: (" + users[i].username + ", " + users[i].password + ", " + users[i].firstname + ", " + users[i].lastname + ")");
                tempStatus = databaseHandler.CreateUser(users[i].username, users[i].firstname, users[i].lastname, users[i].password);
                if (tempStatus)
                    Console.WriteLine("Success");
                else
                    Console.WriteLine("Failure");

                if (i%2 == 0)
                {
                    Console.WriteLine("    Trying to create user: (" + users[i].username + ", " + users[i].password + ", " + users[i].firstname + ", " + users[i].lastname + ")");
                    tempStatus = databaseHandler.CreateUser(users[i].username, users[i].firstname, users[i].lastname, users[i].password);
                    if (tempStatus)
                        Console.WriteLine("    Unexpected succeeded");
                    else
                        Console.WriteLine("    Expected failed");
                }
            }

            Console.WriteLine();
            Console.WriteLine("Getting back users:");
            Console.WriteLine();

            for (int i = 0; i < users.Length; i++)
            {
                userData = databaseHandler.GetUserData(users[i].username);
                if (userData == null)
                    Console.WriteLine("User data null!");
                else
                    printUserData(userData);
                Console.WriteLine();
            }

            return status;
        }

        private static void printUserData(CatShieldServer.UserData userData, string prepend = "")
        {
            Console.WriteLine(prepend + "UserID:      " + userData.UserID);
            Console.WriteLine(prepend + "Username:    " + userData.Username);
            Console.WriteLine(prepend + "Firstname:   " + userData.FirstName);
            Console.WriteLine(prepend + "Lastname:    " + userData.LastName);
        }
    }
}
