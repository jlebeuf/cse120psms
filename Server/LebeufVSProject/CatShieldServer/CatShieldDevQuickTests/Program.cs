﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CatShieldServer;

namespace CatShieldDevQuickTests
{
    class Program
    {
        static int signPort = 26262;
        static int signListenerTimeoutMilliSec = 5000;

        static void Main(string[] args)
        {
            Test[] tests = new Test[]
                {
                    new SignTest(signPort),
                    new SignHandlerTest(signPort, signListenerTimeoutMilliSec),
                    new BroadcastTest(),
                    new GatewayTest(),
                    new DistributionHandlerTest(),
                    new BroadcastQueueTest(),
                    new DatabaseHandlerTest(),
                    new ClientTest(),
                    new ClientHandlerTest()
                };
            Console.WriteLine("Tests (or Q to quit):");
            for (int i = 0; i < tests.Length; i++)
                Console.WriteLine("{0}.\t{1}", i + 1, tests[i].testName);
            string line = Console.ReadLine();
            if (line.ToLower() == "q")
                return;
            int id;
            try
            {
                id = Int32.Parse(line);
            } catch (Exception e)
            {
                Console.WriteLine("Invalid test");
                return;
            }
            if (id < 1 || id > tests.Length+1)
            {
                Console.WriteLine("Invalid test");
                return;
            }
            bool status = tests[id - 1].Run();

            Console.WriteLine("\n\nTest finished");
            if (status)
                Test.SuccessLine("Returned Success");
            else
                Test.FailureLine("Returned Failure");
            Console.WriteLine("Press any key to continue");
            Console.ReadKey();
        }
    }
}
