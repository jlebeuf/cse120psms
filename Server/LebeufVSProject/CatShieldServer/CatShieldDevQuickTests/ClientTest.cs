﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace CatShieldDevQuickTests
{
    class ClientTest : Test
    {
        class ClientEncap : CatShieldServer.Client
        {
            public ClientEncap(CatShieldServer.UserData userData, int sessionID) : base(userData, sessionID) { }
            public void Update() { base.UpdateLastActionTime(); }
        }
        public ClientTest() : base("ClientTest") { }


        public override bool Run()
        {
            bool status = true;

            int seconds = 10;
            TimeSpan timeout = new TimeSpan(0, 0, seconds);
            CatShieldServer.UserData userData = new CatShieldServer.UserData(0, "", "", "", false, false);
            ClientEncap client1 = new ClientEncap(userData, 0);
            for (int i = 0; i < seconds; i++)
            {
                Console.WriteLine("Testing timeout at second " + i);
                if (client1.TimedOut(timeout))
                    FailureLine("Incorrectly reported timeout out");
                else
                    SuccessLine("Correctly reported not timeout out");
                Thread.Sleep(1000);
            }
            Thread.Sleep(1000);
            Console.WriteLine("Now testing after {0} seconds", seconds);
            if (client1.TimedOut(timeout))
                SuccessLine("Correctly reported timeout out");
            else
                FailureLine("Incorrectly reported not timeout out");


            return status;
        }
    }
}
