﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldDevQuickTests
{
    class BroadcastQueueTest : Test
    {

        public BroadcastQueueTest() : base("BroadcastQueueTest") { }


        override public bool Run()
        {
            bool status = true;
            CatShieldServer.DistributionHandler distributionHandler = new CatShieldServer.DistributionHandler();
            CatShieldServer.BroadcastQueue broadcastQueue = new CatShieldServer.BroadcastQueue(distributionHandler);

            CatShieldServer.Gateway[] gateways = new CatShieldServer.Gateway[4];
            for (int i = 0; i < gateways.Length; i++)
            {
                gateways[i] = new CatShieldServer.TestGateway("TestGateway" + (i + 1));
                distributionHandler.AddGateway(gateways[i]);
            }

            string[][] targets = new string[10][];
            targets[0] = new string[] { gateways[0].Name };
            targets[1] = new string[] { gateways[1].Name };
            targets[2] = new string[] { gateways[2].Name };
            targets[3] = new string[] { gateways[3].Name };
            targets[4] = new string[] { gateways[0].Name, gateways[1].Name };
            targets[5] = new string[] { gateways[0].Name, gateways[2].Name };
            targets[6] = new string[] { gateways[0].Name, gateways[3].Name };
            targets[7] = new string[] { gateways[0].Name, gateways[1].Name, gateways[2].Name };
            targets[8] = new string[] { gateways[1].Name, gateways[2].Name };
            targets[9] = new string[] { gateways[2].Name, gateways[3].Name };

            CatShieldServer.Broadcast[] broadcasts = new CatShieldServer.Broadcast[targets.Length];
            for (int i = 0; i < broadcasts.Length; i++)
                broadcasts[i] = new CatShieldServer.Broadcast(i+1, "", "TestMessage"+(i+1), targets[i]);

            for (int i = 0; i < broadcasts.Length; i++)
            {
                broadcastQueue.QueueBroadcast(broadcasts[i]);
                printQueue(broadcastQueue.GetPendingBroadcasts());
            }

            Console.WriteLine("Approving 1");
            broadcastQueue.ApproveBroadcast(1);
            printQueue(broadcastQueue.GetPendingBroadcasts());
            Console.WriteLine("Approving 2");
            broadcastQueue.ApproveBroadcast(2);
            printQueue(broadcastQueue.GetPendingBroadcasts());
            Console.WriteLine("Rejecting 3");
            broadcastQueue.RejectBroadcast(3);
            printQueue(broadcastQueue.GetPendingBroadcasts());
            Console.WriteLine("Approving 3");
            broadcastQueue.ApproveBroadcast(3);
            printQueue(broadcastQueue.GetPendingBroadcasts());
            Console.WriteLine("Approving 4");
            broadcastQueue.ApproveBroadcast(4);
            printQueue(broadcastQueue.GetPendingBroadcasts());
            Console.WriteLine("Rejecting 4");
            broadcastQueue.RejectBroadcast(4);
            printQueue(broadcastQueue.GetPendingBroadcasts());
            Console.WriteLine("Approving 5");
            broadcastQueue.ApproveBroadcast(5);
            printQueue(broadcastQueue.GetPendingBroadcasts());
            Console.WriteLine("Approving 6");
            broadcastQueue.ApproveBroadcast(6);
            printQueue(broadcastQueue.GetPendingBroadcasts());
            Console.WriteLine("Approving 7");
            broadcastQueue.ApproveBroadcast(7);
            printQueue(broadcastQueue.GetPendingBroadcasts());
            Console.WriteLine("Rejecting 8");
            broadcastQueue.RejectBroadcast(8);
            printQueue(broadcastQueue.GetPendingBroadcasts());
            Console.WriteLine("Approving 8");
            broadcastQueue.ApproveBroadcast(8);
            printQueue(broadcastQueue.GetPendingBroadcasts());
            Console.WriteLine("Approving 9");
            broadcastQueue.ApproveBroadcast(9);
            printQueue(broadcastQueue.GetPendingBroadcasts());
            Console.WriteLine("Rejecting 9");
            broadcastQueue.RejectBroadcast(9);
            printQueue(broadcastQueue.GetPendingBroadcasts());
            Console.WriteLine("Approving 9");
            broadcastQueue.ApproveBroadcast(9);
            printQueue(broadcastQueue.GetPendingBroadcasts());

            return status;
        }

        private static void printQueue(CatShieldServer.Broadcast[] broadcasts)
        {
            Console.Write("[");
            for (int i = 0; i < broadcasts.Length; i++)
            {
                if (i > 0)
                    Console.Write(", ");
                Console.Write(broadcasts[i].GetID());
            }
            Console.WriteLine("]");
        }
    }
}
