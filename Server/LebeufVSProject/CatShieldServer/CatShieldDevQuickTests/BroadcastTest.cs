﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldDevQuickTests
{
    class BroadcastTest : Test
    {

        public BroadcastTest() : base("BroadcastTest") { }

        override public bool Run()
        {
            bool status = true;
            string message = "TestMessage";

            string[] gateways = new string[3];
            gateways[0] = "Gateway1";
            gateways[1] = "Gateway2";
            gateways[2] = "Gateway3";

            CatShieldServer.Broadcast broadcast = new CatShieldServer.Broadcast(1, "", message, gateways);

            Console.WriteLine("Broadcast.GetMessage():");
            Console.WriteLine(broadcast.GetMessage());
            Console.WriteLine("Broadcast.GetGateways():");
            Console.WriteLine(broadcast.GetGateways());
            
            Console.WriteLine();
            Console.WriteLine("Gateways:");
            for (int i = 0; i < gateways.Length; i++)
                Console.WriteLine(gateways[i]);
            Console.WriteLine("Broadcast.ToString():");
            Console.WriteLine(broadcast.ToString());

            Console.WriteLine();

            message = "NewTestMessage";
            CatShieldServer.Broadcast broadcast2 = new CatShieldServer.Broadcast(2, "", message, gateways, broadcast);
            Console.WriteLine(broadcast2.ToString());

            return status;
        }
    }
}
