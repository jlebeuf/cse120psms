﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldDevQuickTests
{
    class DistributionHandlerTest : Test
    {

        public DistributionHandlerTest() : base("DistributionHandlerTest") { }


        public override bool Run()
        {
            while (true)
            {
                Console.WriteLine("DistributionHandlerTest Type: ");
                Console.WriteLine("Q. Quit");
                Console.WriteLine("1. Gateway Modification");
                Console.WriteLine("2. Broadcast Pushing");
                string line = Console.ReadLine();
                if (line.ToLower() == "q")
                    return true;
                if (line == "1")
                    return RunGatewayModification();
                if (line == "2")
                    return RunBroadcastPushing();
                Console.WriteLine("Invalid input");
            }
        }

        public bool RunGatewayModification()
        {
            bool status = true;
            CatShieldServer.Gateway gateway1 = new CatShieldServer.TestGateway("TestGateway1");
            CatShieldServer.Gateway gateway2 = new CatShieldServer.TestGateway("TestGateway2");
            CatShieldServer.Gateway gateway3 = new CatShieldServer.TestGateway("TestGateway3");

            CatShieldServer.DistributionHandler distributionHandler = new CatShieldServer.DistributionHandler();

            Console.WriteLine("DistributionHandler.GatewaysToString():");
            Console.WriteLine(distributionHandler.GatewaysToString());
            Console.WriteLine();

            Add(distributionHandler, gateway1);
            Add(distributionHandler, gateway2);
            Add(distributionHandler, gateway3);
            Remove(distributionHandler, gateway2);
            Add(distributionHandler, gateway2);
            Remove(distributionHandler, gateway1);
            Remove(distributionHandler, gateway2);
            Remove(distributionHandler, gateway3);
            Add(distributionHandler, gateway1);
            Remove(distributionHandler, gateway1);
            
            return status;
        }

        public bool RunBroadcastPushing()
        {
            bool status = true;
            int gatewayCount = 4;

            CatShieldServer.DistributionHandler distHand = new CatShieldServer.DistributionHandler();
            for (int i = 0; i < gatewayCount; i++)
                distHand.AddGateway(new CatShieldServer.TestGateway("TestGateway" + i));

            string[][] gateways = new string[7][];
            gateways[0] = new string[] { "TestGateway1" };
            gateways[1] = new string[] { "TestGateway2" };
            gateways[2] = new string[] { "TestGateway3" };
            gateways[3] = new string[] { "TestGateway1", "TestGateway2" };
            gateways[4] = new string[] { "TestGateway1", "TestGateway3" };
            gateways[5] = new string[] { "TestGateway2", "TestGateway3" };
            gateways[6] = new string[] { "TestGateway1", "TestGateway2", "TestGateway3" };

            CatShieldServer.Broadcast[] broadcasts = new CatShieldServer.Broadcast[gateways.Length];
            for (int i = 0; i < gateways.Length; i++)
            {
                broadcasts[i] = new CatShieldServer.Broadcast(i+1, "", "TestMessage" + i, gateways[i]);
                Console.WriteLine(broadcasts[i].ToString());
                distHand.PushBroadcast(broadcasts[i]);
                Console.WriteLine();
            }

            return status;
        }

        private static void Add(CatShieldServer.DistributionHandler distributionHandler, CatShieldServer.Gateway gateway)
        {

            Console.WriteLine("DistributionHandler.AddGateway(" + gateway.ToString() + "):");
            distributionHandler.AddGateway(gateway);
            Console.WriteLine("DistributionHandler.GatewaysToString():");
            Console.WriteLine(distributionHandler.GatewaysToString());
            Console.WriteLine();
        }

        private static void Remove(CatShieldServer.DistributionHandler distributionHandler, CatShieldServer.Gateway gateway)
        {
            Console.WriteLine("DistributionHandler.RemoveGateway(" + gateway.ToString() + "):");
            distributionHandler.RemoveGateway(gateway.Name);
            Console.WriteLine("DistributionHandler.GatewaysToString():");
            Console.WriteLine(distributionHandler.GatewaysToString());
            Console.WriteLine();
        }
    }
}
