﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldDevQuickTests
{
    class SignHandlerTest : Test
    {
        CatShieldServer.SignHandler signHandler;
        int port;
        int listenerTimeout;


        public SignHandlerTest(int _port, int _listenerTimeout) : base("SignHandlerTest")
        {
            port = _port;
            listenerTimeout = _listenerTimeout;
        }

        override public bool Run()
        {
            bool status = true;
            signHandler = new CatShieldServer.SignHandler(port, listenerTimeout);
            string command;

            while (true)
            {
                Console.WriteLine("SignHandlerTest():");
                Console.WriteLine("Q.\tQuit");
                Console.WriteLine("1.\tGet Signs");
                Console.WriteLine("2.\tClear Display");
                Console.WriteLine("3.\tSend Message");
                command = Console.ReadLine();

                if (command.ToLower()[0] == 'q')
                    return status;
                else if (command == "1")
                {
                    CatShieldServer.Sign[] signs = signHandler.GetSigns();
                    if (signs.Length < 1)
                    {
                        Console.WriteLine("No signs");
                        continue;
                    }

                    Console.WriteLine("[ConnectionState, SerialNumber, DisplayLines, DisplayColumns]");
                    foreach (CatShieldServer.Sign s in signs)
                    {
                        Console.WriteLine("[" + s.CurrentConnectionState.ToString() + ", " + s.SerialNumber + ", " + s.DisplayLines + ", " + s.DisplayColumns + "]");
                    }
                }
                else if (command == "2")
                {
                    Console.WriteLine("Sign Serial Number: ");
                    command = Console.ReadLine();
                    int serial;
                    try
                    {
                        serial = Int32.Parse(command);
                    } catch (Exception e)
                    {
                        Console.WriteLine("Invalid serial number");
                        continue;
                    }
                    signHandler.ClearScreen(serial);
                }
                else if (command == "3")
                {
                    Console.WriteLine("Sign Serial Number: ");
                    command = Console.ReadLine();
                    int serial;
                    try
                    {
                        serial = Int32.Parse(command);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Invalid serial number");
                        continue;
                    }
                    Console.WriteLine("Message: ");
                    command = Console.ReadLine();
                    signHandler.SendMessage(serial, command);
                }
                else
                    Console.WriteLine("Invalid input");
            }

            return status;
        }
    }
}
