﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace CatShieldDevQuickTests
{
    class ClientHandlerTest : Test
    {
        class UserEntry
        {
            public string username;
            public string password;
            public string firstname;
            public string lastname;
            public int sessionID = CatShieldServer.ClientHandler.NULL_SESSION_ID;
            public UserEntry(string _un, string _pw, string _fn, string _ln)
            {
                username = _un; password = _pw; firstname = _fn; lastname = _ln;
            }
        }

        public ClientHandlerTest() : base("ClientHandlerTest") { }

        override public bool Run()
        {
            bool status = true;

            CatShieldServer.DatabaseHandler databaseHandler = new CatShieldServer.DatabaseHandler();
            TimeSpan timeout = new TimeSpan(0, 0, 6);
            CatShieldServer.ClientHandler clientHandler = new CatShieldServer.ClientHandler(databaseHandler, timeout);

            Console.WriteLine("Timeout Time: {0}s", timeout.TotalSeconds);
            Console.WriteLine();

            UserEntry[] users = new UserEntry[5];
            for (int i = 1; i <= users.Length; i++)
            {
                users[i-1] = new UserEntry("user"+i, "pass"+i, "first"+i, "last"+i);
                Console.WriteLine("Creating user: {0}, {1}, {2}, {3}", users[i-1].username, users[i-1].password, users[i-1].firstname, users[i-1].lastname);
                databaseHandler.CreateUser(users[i-1].username, users[i-1].firstname, users[i-1].lastname, users[i-1].password);
            }


            //////////////////////////////
            //////////////////// Timeout
            //////////////////////////////
            Console.WriteLine("Logging in users");
            for (int i = 0; i < users.Length; i++)
            {
                users[i].sessionID = clientHandler.LoginUser(users[i].username, users[i].password);
                if (users[i].sessionID != CatShieldServer.ClientHandler.NULL_SESSION_ID)
                    SuccessLine("Successed logging in user: " + users[i].username);
                else
                {
                    FailureLine("Failed to login user: " + users[i].username);
                    status = false;
                }
            }

            Console.WriteLine("Sleeping half of timeout time");
            for (int i = 0; i < (int)timeout.TotalSeconds / 2; i++)
            {
                Console.Write(i + " ");
                Thread.Sleep(1000);
            }
            Console.WriteLine("Continuing");

            Console.WriteLine("Checking still logged in (no refresh)");
            for (int i = 0; i < users.Length; i++)
            {
                if (clientHandler.LoggedInUser(users[i].sessionID, false))
                    SuccessLine("User correctly still logged in: " + users[i].username);
                else
                {
                    FailureLine("User incorrectly logged out: " + users[i].username);
                    status = false;
                }
            }

            Console.WriteLine("Sleeping another half of timeout time + 0.25s");
            for (int i = 0; i < (int)timeout.TotalSeconds / 2; i++)
            {
                Console.Write(i + " ");
                Thread.Sleep(1000);
            }
            Thread.Sleep(250);
            Console.WriteLine("Continuing");

            Console.WriteLine("Checking users logged out");
            for (int i = 0; i < users.Length; i++)
            {
                if (!clientHandler.LoggedInUser(users[i].sessionID))
                    SuccessLine("User correctly logged out: " + users[i].username);
                else
                {
                    FailureLine("User incorrectly logged in: " + users[i].username);
                    status = false;
                }
            }


            //////////////////////////////
            //////////////////// Logout
            //////////////////////////////
            Console.WriteLine("Logging in users");
            for (int i = 0; i < users.Length; i++)
            {
                users[i].sessionID = clientHandler.LoginUser(users[i].username, users[i].password);
                if (users[i].sessionID != CatShieldServer.ClientHandler.NULL_SESSION_ID)
                    SuccessLine("Successed logging in user: " + users[i].username);
                else
                {
                    FailureLine("Failed to login user: " + users[i].username);
                    status = false;
                }
            }

            Console.WriteLine("Checking users logged in");
            for (int i = 0; i < users.Length; i++)
            {
                if (clientHandler.LoggedInUser(users[i].sessionID, false))
                    SuccessLine("User still logged in: " + users[i].username);
                else
                {
                    FailureLine("User incorrectly logged out: " + users[i].username);
                    status = false;
                }
            }

            Console.WriteLine("Sleeping half of timeout time");
            for (int i = 0; i < (int)timeout.TotalSeconds / 2; i++)
            {
                Console.Write(i + " ");
                Thread.Sleep(1000);
            }
            Console.WriteLine("Continuing");

            Console.WriteLine("Checking still logged in");
            for (int i = 0; i < users.Length; i++)
            {
                if (clientHandler.LoggedInUser(users[i].sessionID))
                    SuccessLine("User correctly still logged in: " + users[i].username);
                else
                {
                    FailureLine("User incorrectly logged out: " + users[i].username);
                    status = false;
                }
            }

            Console.WriteLine("Logging users out");
            for (int i = 0; i < users.Length; i++)
            {
                bool loggedOut = clientHandler.LogoutUser(users[i].sessionID);
                if (loggedOut)
                    SuccessLine("User successfully logged out: " + users[i].username);
                else
                {
                    FailureLine("User logout failed: " + users[i].username);
                    status = false;
                }
            }

            Console.WriteLine("Checking users logged out");
            for (int i = 0; i < users.Length; i++)
            {
                if (!clientHandler.LoggedInUser(users[i].sessionID))
                    SuccessLine("User correctly logged out: " + users[i].username);
                else
                {
                    FailureLine("User incorrectly logged in: " + users[i].username);
                    status = false;
                }
            }


            //////////////////////////////
            //////////////////// Logged in refresh
            //////////////////////////////
            Console.WriteLine("Logging in users");
            for (int i = 0; i < users.Length; i++)
            {
                users[i].sessionID = clientHandler.LoginUser(users[i].username, users[i].password);
                if (users[i].sessionID != CatShieldServer.ClientHandler.NULL_SESSION_ID)
                    SuccessLine("Successed logging in user: " + users[i].username);
                else
                {
                    FailureLine("Failed to login user: " + users[i].username);
                    status = false;
                }
            }

            Console.WriteLine("Sleeping 3/4 of timeout time");
            for (int i = 0; i < (int)timeout.TotalSeconds * 3 / 4; i++)
            {
                Console.Write(i + " ");
                Thread.Sleep(1000);
            }
            Console.WriteLine("Continuing");

            Console.WriteLine("Checking still logged in");
            for (int i = 0; i < users.Length; i++)
            {
                if (clientHandler.LoggedInUser(users[i].sessionID, true))
                    SuccessLine("User correctly still logged in: " + users[i].username);
                else
                {
                    FailureLine("User incorrectly logged out: " + users[i].username);
                    status = false;
                }
            }

            Console.WriteLine("Sleeping 3/4 of timeout time");
            for (int i = 0; i < (int)timeout.TotalSeconds * 3 / 4; i++)
            {
                Console.Write(i + " ");
                Thread.Sleep(1000);
            }
            Console.WriteLine("Continuing");

            Console.WriteLine("Checking still logged in");
            for (int i = 0; i < users.Length; i++)
            {
                if (clientHandler.LoggedInUser(users[i].sessionID))
                    SuccessLine("User correctly still logged in: " + users[i].username);
                else
                {
                    FailureLine("User incorrectly logged out: " + users[i].username);
                    status = false;
                }
            }

            Console.WriteLine("Sleeping full timeout time + 0.25s");
            for (int i = 0; i < (int)timeout.TotalSeconds; i++)
            {
                Console.Write(i + " ");
                Thread.Sleep(1000);
            }
            Thread.Sleep(250);
            Console.WriteLine("Continuing");

            Console.WriteLine("Checking users logged out");
            for (int i = 0; i < users.Length; i++)
            {
                if (!clientHandler.LoggedInUser(users[i].sessionID))
                    SuccessLine("User correctly logged out: " + users[i].username);
                else
                {
                    FailureLine("User incorrectly logged in: " + users[i].username);
                    status = false;
                }
            }

            /*CatShieldServer.DatabaseHandler databaseHandler = new CatShieldServer.DatabaseHandler();
            CatShieldServer.DistributionHandler distributionHandler = new CatShieldServer.DistributionHandler();
            CatShieldServer.BroadcastQueue broadcastQueue = new CatShieldServer.BroadcastQueue(distributionHandler);
            CatShieldServer.SignHandler signHandler = new CatShieldServer.SignHandler(26262, 1000);
            CatShieldServer.ClientHandler clientHandler = new CatShieldServer.ClientHandler(databaseHandler, new TimeSpan(0, 15, 0));

            UserEntry[] users = new UserEntry[5];
            CatShieldServer.Client[] clients = new CatShieldServer.Client[users.Length];
            CatShieldServer.Client tempClient;
            bool tempBool;

            for (int i = 1; i <= users.Length; i++)
            {
                users[i-1] = new UserEntry("user"+i, "pass"+i, "first"+i, "last"+i);
                databaseHandler.CreateUser(users[i-1].username, users[i-1].firstname, users[i-1].lastname, users[i-1].password);
            }

            Console.WriteLine("Trying to login 'FakeUsername', 'FakePassword'");
            tempClient = clientHandler.LoginClient("FakeUsername", "FakePassword");
            if (tempClient != null)
                status = FailureLine("Incorrectly received a client");
            else
                SuccessLine("Correctly received null client");

            Console.WriteLine("Trying to login '{0}', 'FakePassword'", users[0].username);
            tempClient = clientHandler.LoginClient(users[0].username, "FakePassword");
            if (tempClient != null)
                status = FailureLine("Incorrectly received a client");
            else
                SuccessLine("Correctly received null client");

            Console.WriteLine();
            Console.WriteLine();

            for (int i = 0; i < users.Length; i++)
            {
                Console.WriteLine("Logging in: " + users[i].username);
                clients[i] = clientHandler.LoginClient(users[i].username, users[i].password);
                if (clients[i] == null)
                    status = FailureLine("Login Failed!");
                else
                    SuccessLine("Login Success, session ID: " + clients[i].SessionID);
            }

            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("Trying to logout 'FakeUsername'");
            tempBool = clientHandler.LogoutClient("FakeUsername");
            if (tempBool)
                status = FailureLine("Incorrectly logged out user");
            else
                SuccessLine("Correctly failed logout");

            Console.WriteLine("Trying to logout -10 userID");
            tempBool = clientHandler.LogoutClient(-10);
            if (tempBool)
                status = FailureLine("Incorrectly logged out user");
            else
                SuccessLine("Correctly failed logout");

            Console.WriteLine("Trying to logout null Client");
            tempBool = clientHandler.LogoutClient((CatShieldServer.Client)null);
            if (tempBool)
                status = FailureLine("Incorrectly logged out user");
            else
                SuccessLine("Correctly failed logout");

            tempClient = new CatShieldServer.Client(new CatShieldServer.UserData(0, "", "", ""), 0);
            Console.WriteLine("Trying to logout self made Client");
            tempBool = clientHandler.LogoutClient(tempClient);
            if (tempBool)
                status = FailureLine("Incorrectly logged out user");
            else
                SuccessLine("Correctly failed logout");
            
            Console.WriteLine();
            Console.WriteLine();

            for (int i = 0; i < clients.Length; i++)
            {
                if (i == 0)
                {
                    Console.WriteLine("Logging out username {0}", users[i].username);
                    tempBool = clientHandler.LogoutClient(users[i].username);
                    if (tempBool)
                        SuccessLine("Correctly logged out user");
                    else
                        status = FailureLine("Incorrectly failed logout");
                }
                else if (i == 1)
                {
                    Console.WriteLine("Logging out clients["+i+"].username {0}", users[i].username);
                    tempBool = clientHandler.LogoutClient(clients[i]);
                    if (tempBool)
                        SuccessLine("Correctly logged out user");
                    else
                        status = FailureLine("Incorrectly failed logout");
                }
                else
                {
                    Console.WriteLine("Logging out client ({0})", users[i].username);
                    tempBool = clientHandler.LogoutClient(clients[i]);
                    if (tempBool)
                        SuccessLine("Correctly logged out user");
                    else
                        status = FailureLine("Incorrectly failed logout");
                }
            }

            Console.WriteLine();
            Console.WriteLine();

            for (int i = 0; i < clients.Length; i++)
            {
                if (i == 0)
                {
                    Console.WriteLine("Logging out username {0}", users[i].username);
                    tempBool = clientHandler.LogoutClient(users[i].username);
                    if (tempBool)
                        status = FailureLine("Incorrectly logged out user");
                    else
                        SuccessLine("Correctly failed logout");
                }
                else if (i == 1)
                {
                    Console.WriteLine("Logging out clients[" + i + "].username {0}", users[i].username);
                    tempBool = clientHandler.LogoutClient(clients[i]);
                    if (tempBool)
                        status = FailureLine("Incorrectly logged out user");
                    else
                        SuccessLine("Correctly failed logout");
                }
                else
                {
                    Console.WriteLine("Logging out client ({0})", users[i].username);
                    tempBool = clientHandler.LogoutClient(clients[i]);
                    if (tempBool)
                        status = FailureLine("Incorrectly logged out user");
                    else
                        SuccessLine("Correctly failed logout");
                }
            }//*/

            return status;
        }
    }
}
