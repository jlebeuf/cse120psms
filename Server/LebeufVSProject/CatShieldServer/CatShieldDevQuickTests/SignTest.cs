﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace CatShieldDevQuickTests
{
    class SignTest : Test
    {
        int port;
        Thread listenerThread;
        int failWaitMilliseconds = 5000;
        LinkedList<CatShieldServer.Sign> connectedSigns;

        public SignTest(int _port) : base("SignTest")
        {
            port = _port;
        }

        override public bool Run()
        {
            bool status = true;
            connectedSigns = new LinkedList<CatShieldServer.Sign>();
            listenerThread = new Thread(new ThreadStart(listenerThreadMethod));
            listenerThread.Start();
            string message;

            while (true)
            {
                Console.WriteLine("TestSignConnection():");
                Console.WriteLine("1.\tClear Screens");
                Console.WriteLine("2.\tSend Message");
                Console.WriteLine("Q.\tQuit");
                message = Console.ReadLine();

                if (message == "1")
                    SendClearScreen();
                else
                if (message == "2")
                {
                    Console.Write("Message: ");
                    message = Console.ReadLine();
                    SendMessage(message);
                }
                else
                if (message.ToLower() == "q")
                    break;
                else
                    Console.WriteLine("Invalid input");
            }

            return status;
        }

        public void SendClearScreen()
        {
            foreach (CatShieldServer.Sign sign in connectedSigns)
                sign.SendClearScreen();
        }

        public void SendMessage(string message)
        {
            foreach (CatShieldServer.Sign sign in connectedSigns)
                sign.SendMessage(message);
        }


        private void listenerThreadMethod()
        {
            TcpListener listener = null;
            bool started = false;
            while (!started)
            {
                Console.WriteLine("Creating TcpListener");

                try
                {
                    listener = new TcpListener(IPAddress.Any, port);
                    listener.Start();
                    started = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed to create TcpListener on port {0}", port);
                    Console.WriteLine("Error: {0}", e.Message);
                    Console.WriteLine("Waiting for {0} seconds before trying again", failWaitMilliseconds/1000);
                    Thread.Sleep(failWaitMilliseconds);
                }
            }

            Console.WriteLine("TcpListener created on port {0}", port);

            while (true)
            {
                Console.WriteLine("\n\nWaiting for connection");
                TcpClient tcpClient = listener.AcceptTcpClient();
                Console.WriteLine("Connection accepted, saving it");
                CatShieldServer.Sign newSign = new CatShieldServer.Sign(tcpClient);
                connectedSigns.AddLast(newSign);
            }
        }
    }
}
