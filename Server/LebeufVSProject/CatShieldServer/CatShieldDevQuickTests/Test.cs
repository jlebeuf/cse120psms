﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldDevQuickTests
{
    abstract class Test
    {
        public string testName;

        public Test(string _testName)
        {
            testName = _testName;
        }

        abstract public bool Run();

        
        public static void NoticeLine(string message)
        {
            Notice(message + "\n");
        }

        public static void Notice(string message)
        {
            Console.ResetColor();
            Console.Write(message);
        }

        public static bool SuccessLine(string message)
        {
            Success(message + "\n");
            return true;
        }

        public static bool Success(string message)
        {
            Console.BackgroundColor = ConsoleColor.DarkGreen;
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(message);
            Console.ResetColor();
            return true;
        }

        public static bool FailureLine(string message)
        {
            Failure(message + "\n");
            return false;
        }

        public static bool Failure(string message)
        {
            Console.BackgroundColor = ConsoleColor.DarkRed;
            Console.ForegroundColor = ConsoleColor.White;
            Console.Write(message);
            Console.ResetColor();
            return false;
        }
    }
}
