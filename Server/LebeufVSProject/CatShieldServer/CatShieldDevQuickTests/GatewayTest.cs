﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldDevQuickTests
{
    class GatewayTest : Test
    {

        public GatewayTest() : base("GatewayTest") { }


        override public bool Run()
        {
            bool status = false;
            string message = "TestMessage";
            string[] gateways1 = new string[1];
            gateways1[0] = "TestGateway";
            string[] gateways2 = new string[1];
            gateways2[0] = "AnotherGateway";

            CatShieldServer.Broadcast broadcast1 = new CatShieldServer.Broadcast(1, "", message, gateways1);
            CatShieldServer.Broadcast broadcast2 = new CatShieldServer.Broadcast(2, "", message, gateways2);

            CatShieldServer.TestGateway testGateway = new CatShieldServer.TestGateway();

            testGateway.PushBroadcast(broadcast1);
            testGateway.PushBroadcast(broadcast2);

            return status;
        }
    }
}
