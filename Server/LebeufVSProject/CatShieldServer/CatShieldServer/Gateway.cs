﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace CatShieldServer
{
    abstract class Gateway
    {
        private Thread processThread = null;
        private LinkedList<Broadcast> pendingBroadcasts;
        private const int PENDING_CHECK_SLEEP_TIME = 1000;

        protected readonly string name;
        /// <summary>
        /// Name of gateway
        /// </summary>
        public string Name { get { return name; } }


        /// <summary>
        /// Creates a new gateway with given name
        /// </summary>
        /// <param name="_name">Name to identify gateway</param>
        public Gateway(string _name)
        {
            name = _name;
            pendingBroadcasts = new LinkedList<Broadcast>();
            processThread = new Thread(new ThreadStart(ProcessThreadMethod));
            processThread.IsBackground = true;
            processThread.Start();
        }


        /// <summary>
        /// Checks if given broadcast targets gateway
        /// </summary>
        /// <param name="broadcast">Broadcast to check</param>
        /// <returns>True if broadcast targets gateway, false otherwise</returns>
        public bool ReceivesBroadcast(Broadcast broadcast)
        {
            return broadcast.HasGateway(name);
        }

        /// <summary>
        /// Method to be overriden by gateway to handle broadcast
        /// </summary>
        /// <param name="broadcast">Broadcast to handle</param>
        public void PushBroadcast(Broadcast broadcast)
        {
            lock (pendingBroadcasts)
            {
                pendingBroadcasts.AddLast(broadcast);
            }
        }


        abstract protected bool ProcessBroadcast(Broadcast broadcast);


        private void ProcessThreadMethod()
        {
            while (true)
            {
                Broadcast broadcast = null;
                lock (pendingBroadcasts)
                {
                    if (pendingBroadcasts.Count > 0)
                    {
                        broadcast = pendingBroadcasts.First.Value;
                        pendingBroadcasts.RemoveFirst();
                    }
                }
                if (broadcast != null)
                {
                    if (ProcessBroadcast(broadcast))
                        Console.WriteLine(Name + " broadcast success: " + broadcast.ToString());
                    else
                        Console.WriteLine(Name + " broadcast failure: " + broadcast.ToString());
                }
                else
                    Thread.Sleep(PENDING_CHECK_SLEEP_TIME);
            }
        }

        /// <summary>
        /// Creates a string representing gateway
        /// </summary>
        /// <returns>String representing gateway</returns>
        public override string ToString()
        {
            return "[" + Name + "]";
        }
    }
}
