﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SQLite;

namespace CatShieldServer
{
    internal class DatabaseHandler : IEmailRecipientProvider, ISMSRecipientProvider
    {
        class UserEntry
        {
            static int nextID = 0;
            public int userID;
            public string username;
            public string password;
            public string firstname;
            public string lastname;
            public bool admin;
            public bool canApproveAndRejectBroadcasts;
            public UserEntry(string _username, string _password, string _firstname, string _lastname)
            {
                userID = nextID++; username = _username; password = _password; firstname = _firstname; lastname = _lastname; admin = false; canApproveAndRejectBroadcasts = false;
            }
            public UserEntry(string _username, string _password, string _firstname, string _lastname, bool _admin, bool _canApproveAndRejectBroadcasts)
            {
                userID = nextID++; username = _username; password = _password; firstname = _firstname; lastname = _lastname; admin = _admin; canApproveAndRejectBroadcasts = _canApproveAndRejectBroadcasts;
            }
        }
        private LinkedList<UserEntry> users;

        private int nextBroadcastID;

        private SQLiteConnection dbConnection;


        public DatabaseHandler()
        {
            users = new LinkedList<UserEntry>();
            UserEntry entry = new UserEntry("admin", "admin", "admin", "admin", true, true);
            users.AddLast(entry);

            nextBroadcastID = 1;

            dbConnection = new SQLiteConnection("Data Source=database.sqlite;Version=3;");
            dbConnection.Open();
        }



        private UserEntry FindUserEntry(string username)
        {
            foreach (UserEntry ue in users)
                if (ue.username == username)
                    return ue;
            return null;
        }
        
        public bool ValidateCredentials(string username, string password)
        {
            try
            {
                string sql = "SELECT username"
                              + " FROM Users"
                              + " WHERE username = '" + username + "'"
                              + " AND password = '" + password + "'";
                SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
                SQLiteDataReader reader = command.ExecuteReader();
                if (!reader.Read())
                    return false;
                else
                    return true;
            }
            catch (Exception e)
            {
                DebugLog.WriteLine("Failed to read user from database");
                return false;
            }
        }

        public bool CreateUser(string username, string firstname, string lastname, string password)
        {
            return CreateUser(username, password, firstname, lastname, false, false);
        }

        public bool CreateUser(string username, string firstname, string lastname, string password, bool isAdmin, bool canApproveAndRejectBroadcasts)
        {
            try
            {
                string sql = "INSERT INTO Users"
                            + " (username, firstName, lastName, password, isAdmin, broadcastApproveReject)"
                            + " VALUES"
                            + " ('" + username + "', '" + firstname + "', '" + lastname + "', '" + password + "', '" + isAdmin + "', '" + canApproveAndRejectBroadcasts + "')";
                SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
                SQLiteDataReader reader = command.ExecuteReader();
                if (reader.RecordsAffected < 1)
                    return false;
                else
                    return true;
            }
            catch (Exception e)
            {
                DebugLog.WriteLine("Failed to add user to database");
                return false;
            }
        }

        public bool UpdatePassword(string username, string currentPassword, string newPassword)
        {
            try
            {
                string sql = "UPDATE Users"
                            + " SET password = '" + newPassword + "'"
                            + " WHERE username = '" + username + "'"
                            + " AND password = '" + currentPassword + "'";
                SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
                SQLiteDataReader reader = command.ExecuteReader();
                if (reader.RecordsAffected < 1)
                    return false;
                else
                    return true;
            }
            catch (Exception e)
            {
                DebugLog.WriteLine("Failed to user password in database");
                return false;
            }
        }

        public UserData GetUserData(string username)
        {
            try
            {
                string sql = "SELECT userID, username, firstName, lastName, active, isAdmin, broadcastApproveReject"
                              + " FROM Users"
                              + " WHERE username = '" + username + "'";
                SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
                SQLiteDataReader reader = command.ExecuteReader();
                if (!reader.Read())
                    return null;

                int userID;
                if (!Int32.TryParse(reader["userID"].ToString(), out userID))
                    return null;
                bool isAdmin = (bool)reader["isAdmin"];
                bool canApproveReject = (bool)reader["broadcastApproveReject"];
                return new UserData(userID, reader["username"].ToString(), reader["firstName"].ToString(), reader["lastName"].ToString(), isAdmin, canApproveReject);
            }
            catch (Exception e)
            {
                DebugLog.WriteLine("Failed to read user from database");
                return null;
            }
        }

        public UserData ValidateAndGetUserData(string username, string password)
        {
            try
            {
                string sql = "SELECT userID, username, firstName, lastName, active, isAdmin, broadcastApproveReject"
                              + " FROM Users"
                              + " WHERE username = '" + username + "'"
                              + " AND password = '" + password + "'";
                SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
                SQLiteDataReader reader = command.ExecuteReader();
                if (!reader.Read())
                    return null;

                int userID;
                if (!Int32.TryParse(reader["userID"].ToString(), out userID))
                    return null;
                bool isAdmin = (bool)reader["isAdmin"];
                bool canApproveReject = (bool)reader["broadcastApproveReject"];
                return new UserData(userID, reader["username"].ToString(), reader["firstName"].ToString(), reader["lastName"].ToString(), isAdmin, canApproveReject);
            }
            catch (Exception e)
            {
                DebugLog.WriteLine("Failed to read user from database");
                return null;
            }
        }

        public int ClaimNextBroadcastID()
        {
            try
            {
                SQLiteCommand command = dbConnection.CreateCommand();
                SQLiteTransaction transaction = null;

                transaction = dbConnection.BeginTransaction();

                command.Transaction = transaction;

                command.CommandText = "INSERT INTO Broadcasts (status) VALUES ('CREATING')";
                command.ExecuteNonQuery();

                command.CommandText = "SELECT last_insert_rowid()";
                SQLiteDataReader reader = command.ExecuteReader();

                transaction.Commit();
                if (!reader.Read())
                    return -1;
                string rowIDStr = reader[0].ToString();
                int rowID;
                if (Int32.TryParse(rowIDStr, out rowID))
                    return rowID;
                else
                    return -1;
            }
            catch (Exception e)
            {
                DebugLog.WriteLine("Failed to read email recipients from database");
                return -1;
            }
        }

        public string[] GetEmailRecipients()
        {
            try
            {
                LinkedList<string> strs = new LinkedList<string>();
                string sql = "SELECT * FROM EmailRecipients WHERE active = 1";
                SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    strs.AddLast(reader["address"].ToString());
                }
                return strs.ToArray<string>();
            } catch (Exception e)
            {
                DebugLog.WriteLine("Failed to read email recipients from database");
                return new string[0];
            }
        }

        public string[] GetSMSRecipientsAsEmail()
        {
            try
            {
                LinkedList<string> strs = new LinkedList<string>();
                string sql = "SELECT number || '@' || emailGateway AS address FROM SMSRecipients, SMSProviders WHERE SMSRecipients.provider = SMSProviders.provider AND active = 1";
                SQLiteCommand command = new SQLiteCommand(sql, dbConnection);
                SQLiteDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    strs.AddLast(reader["address"].ToString());
                }
                return strs.ToArray<string>();
            } catch (Exception e)
            {
                DebugLog.WriteLine("Failed to read email recipients from database");
                return new string[0];
            }
        }
    }
}
