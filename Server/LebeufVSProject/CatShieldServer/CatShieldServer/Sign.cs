﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace CatShieldServer
{
    public class Sign
    {
        // TODO: state change delegate/callback
        public delegate void ConnectionStateChange(Sign sign);
        public ConnectionStateChange stateChangeDelegate;

        /// <summary>
        /// Possible states of connection to sign
        /// </summary>
        public enum ConnectionState
        {
            Connecting,
            Connected,
            Disconnected
        }

        /// <summary>
        /// Current sign connection state
        /// </summary>
        private ConnectionState connectionState;
        public ConnectionState CurrentConnectionState { get { return connectionState; } }

        /// <summary>
        /// Serial Number of the sign
        /// </summary>
        private int serialNumber;
        public int SerialNumber { get { return serialNumber; } }

        /// <summary>
        /// Amount of lines display has
        /// </summary>
        private int displayLines;
        public int DisplayLines { get { return displayLines; } }

        /// <summary>
        /// Amount of columns display has for each line
        /// </summary>
        private int displayColumns;
        public int DisplayColumns { get { return displayColumns; } }

        /// <summary>
        /// Connection to sign
        /// </summary>
        private TcpClient tcpClient;
        // Probably shouldn't expose tcpClient through a getter

        /// <summary>
        /// Stream from connection to client. Saved to reduce calls to connections stream getter
        /// </summary>
        private NetworkStream stream;

        /// <summary>
        /// Thread to monitor sign connection state
        /// </summary>
        private Thread connectionMonitorThread;

        /// <summary>
        /// Creates a sign from a freshly accepted TCP connection
        /// </summary>
        /// <param name="_tcpClient">TCP connection to sign</param>
        public Sign(TcpClient _tcpClient)
        {
            serialNumber = -1;
            displayLines = -1;
            displayColumns = -1;
            tcpClient = _tcpClient;
            if (tcpClient != null)
            {
                stream = tcpClient.GetStream();
                connectionState = ConnectionState.Connecting;
            }
            else
            {
                stream = null;
                connectionState = ConnectionState.Disconnected;
            }
            connectionMonitorThread = new Thread(new ThreadStart(MonitorConnectionThreadMethod));
            connectionMonitorThread.IsBackground = true;
            connectionMonitorThread.Start();
        }


        private bool SignalStateChange(ConnectionState newState)
        {
            if (connectionState == newState)
                return false;

            connectionState = newState;
            if (stateChangeDelegate != null)
                stateChangeDelegate(this);
            return true;
        }


        /// <summary>
        /// Method for connection monitor thread to continuously check state of connection
        /// </summary>
        private void MonitorConnectionThreadMethod()
        {
            while (true)
            {
                if (connectionState == ConnectionState.Connecting)
                {
                    byte[] serialBytes = new byte[4];
                    int serialBytesPos = 3;
                    int clientLines = 0;
                    int clientColumns = 0;
                    for (int i = 0; i < 7; i++)
                    {
                        int j = -1;
                        try
                        {
                            while (j < 0)
                                j = stream.ReadByte();
                        }
                        catch (Exception e)
                        {

                        }

                        if (i == 0)
                            continue; // first command byte
                        if (i < 5)
                            serialBytes[serialBytesPos--] = (byte)j;
                        else
                            if (i == 5)
                                clientLines = j;
                            else
                                clientColumns = j;
                    }
                    serialNumber = BitConverter.ToInt32(serialBytes, 0);
                    displayLines = clientLines;
                    displayColumns = clientColumns;
                    SignalStateChange(ConnectionState.Connected);
                }

                if (connectionState == ConnectionState.Connected)
                {
                    bool poll = false;
                    try
                    {
                        poll = tcpClient.Client.Poll(0, SelectMode.SelectRead);
                    } catch (Exception e)
                    {
                        SignalStateChange(ConnectionState.Disconnected);
                    }

                    if (poll)
                    {
                        byte[] b = new byte[1];
                        try
                        {
                            if (tcpClient.Client.Receive(b, SocketFlags.Peek) == 0)
                                SignalStateChange(ConnectionState.Disconnected);
                        }
                        catch (Exception e)
                        {
                            SignalStateChange(ConnectionState.Disconnected);
                        }
                    }
                }


                Thread.Sleep(0); // Sleep for half second
            }
        }


        /// <summary>
        /// Send clear screen command to sign
        /// </summary>
        public void SendClearScreen()
        {
            byte[] bytesToSend = new byte[2];
            bytesToSend[0] = 0x01;
            bytesToSend[1] = 0x00;

            try
            {
                stream.Write(bytesToSend, 0, bytesToSend.Length);
            }
            catch (Exception e)
            {
                DebugLog.WriteLine("Failed to write to client, disconnected?");
            }
        }

        /// <summary>
        /// Send message to sign
        /// </summary>
        /// <param name="message">Message to send</param>
        public void SendMessage(string message)
        {
            byte messageLength = (byte)message.Length;
            byte[] msgASCIIBytes = ASCIIEncoding.ASCII.GetBytes(message);
            byte[] msgBytes = new byte[messageLength];
            int msgBytesLength = 0;
            for (int i = 0; i < messageLength; i++)
            {
                if (message[i] == '\\')
                {
                    if (i + 1 < messageLength && message[i + 1] == '\\')
                    {
                        ASCIIEncoding.ASCII.GetBytes(message, i, 1, msgBytes, msgBytesLength);
                        i++; // Two characters making one character ('\\' -> '\')
                    }
                    else
                    {
                        // '\' -> null byte (0x00)
                        msgBytes[msgBytesLength] = 0x00;
                    }
                }
                else
                {
                    ASCIIEncoding.ASCII.GetBytes(message, i, 1, msgBytes, msgBytesLength);
                }
                msgBytesLength++;
            }

            byte[] bytesToSend = new byte[3 + msgBytesLength];
            bytesToSend[0] = 0x07;
            bytesToSend[1] = (byte)msgBytesLength;
            for (int i = 0; i < msgBytesLength; i++)
                bytesToSend[2 + i] = msgBytes[i];
            bytesToSend[bytesToSend.Length - 1] = 0x00;

            try
            {
                stream.Write(bytesToSend, 0, bytesToSend.Length);
            }
            catch (Exception e)
            {
                DebugLog.WriteLine("Failed to write to client, disconnected?");
            }
        }

        public override string ToString()
        {
            return "[" + serialNumber + ": {" + connectionState.ToString() + "}, {" + displayLines + " x " + displayColumns + "}]";
        }
    }
}
