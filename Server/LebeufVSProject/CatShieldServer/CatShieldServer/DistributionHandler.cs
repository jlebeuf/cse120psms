﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldServer
{
    class DistributionHandler
    {
        LinkedList<Gateway> gateways;


        /// <summary>
        /// Creates a new distribution handler
        /// </summary>
        public DistributionHandler()
        {
            gateways = new LinkedList<Gateway>();
        }


        /// <summary>
        /// Adds given gateway to gateways
        /// </summary>
        /// <param name="_gateway">Gateway to add</param>
        public void AddGateway(Gateway _gateway)
        {
            gateways.AddLast(_gateway);
        }

        /// <summary>
        /// Removes given gateway from gateways
        /// </summary>
        /// <param name="_gatewayName">Gateway to remove</param>
        public void RemoveGateway(string _gatewayName)
        {
            lock (gateways)
            {
                LinkedListNode<Gateway> node = gateways.First;
                while (node != null)
                {
                    LinkedListNode<Gateway> next = node.Next;
                    if (node.Value.Name == _gatewayName)
                        gateways.Remove(node);
                    node = next;
                }
            }
        }

        /// <summary>
        /// Handles given broadcast
        /// </summary>
        /// <param name="broadcast">Broadcast to handle</param>
        public void PushBroadcast(Broadcast broadcast)
        {
            lock (gateways)
            {
                foreach (Gateway g in gateways)
                {
                    if (g.ReceivesBroadcast(broadcast))
                        g.PushBroadcast(broadcast);
                }
            }
        }

        /// <summary>
        /// Creates a string representing gateways
        /// </summary>
        /// <returns>String representing gateways</returns>
        public string GatewaysToString()
        {
            string result = "[";

            lock (gateways)
            {
                bool first = true;
                foreach (Gateway g in gateways)
                {
                    if (!first)
                        result += ", ";
                    else
                        first = false;
                    result += g.Name;
                }
            }

            result += "]";

            return result;
        }

        public string[] GatewaysToStringArray()
        {
            string[] result;
            int i = 0;
            lock (gateways)
            {
                result = new string[gateways.Count];
                foreach (Gateway g in gateways)
                    result[i++] = g.Name;
            }

            return result;
        }
    }
}
