﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CatShieldServer
{
    class SMSGateway : Gateway
    {
        ISMSRecipientProvider provider;


        public SMSGateway(ISMSRecipientProvider _provider) :
            base("SMS")
        {
            provider = _provider;
        }

        protected override bool ProcessBroadcast(Broadcast broadcast)
        {
            string[] recipients = provider.GetSMSRecipientsAsEmail();
            try
            {
                SmtpClient client = new SmtpClient("pod51011.outlook.com", 587);
                MailAddress self = new MailAddress("catshielddev@ucmerced.edu", "CatShield Broadcast System");
                MailAddress to = new MailAddress("catshieldtesting@jlebeuf.com", "Cat Shield Tester");
                MailMessage message = new MailMessage(self, self);
                for (int i = 0; i < recipients.Length; i++)
                    message.Bcc.Add(new MailAddress(recipients[i]));
                message.Body = broadcast.GetMessage();
                message.Subject = "CatShield Broadcast Test";
                client.Credentials = new System.Net.NetworkCredential("catshielddev@ucmerced.edu", "UCM2015Cat$hieldUCMEMAIL");
                client.EnableSsl = true;
                client.Send(message);
                return true;
            } catch (Exception e)
            {
                return false;
            }
        }
    }
}
