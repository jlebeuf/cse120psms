﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldServer
{
    public interface IUserInterface
    {

        //////////////////////////////
        ////////// SESSION MANAGEMENT
        //////////////////////////////
        /// <summary>
        /// ID of null/invalid session
        /// </summary>
        int NULL_SESSION_ID { get; }

        /// <summary>
        /// Logs a user in or acquires existing sessionID if already logged in and correct password
        /// </summary>
        /// <param name="username">User to log in</param>
        /// <param name="password">Password for user</param>
        /// <returns>ID of login session</returns>
        /// <remarks>Check returned ID against null ID</remarks>
        int Login(string username, string password);
        
        /// <summary>
        /// Checks if user session ID is valid (e.g. not timed out). Renews login timeout countdown
        /// </summary>
        /// <param name="sessionID">ID of user login session</param>
        /// <returns>True if logged in, false otherwise (e.g. timed out)</returns>
        bool LoggedIn(int sessionID);

        /// <summary>
        /// Gets user data for session ID or null if logged/timed out. Renews login timeout countdown
        /// </summary>
        /// <param name="sessionID">ID of login session</param>
        /// <returns>UserData of logged in user, otherwise null</returns>
        UserData GetUserData(int sessionID);

        /// <summary>
        /// Logs a user out
        /// </summary>
        /// <param name="sessionID">ID of login session to close</param>
        /// <returns>True if logout successful</returns>
        bool Logout(int sessionID);



        //////////////////////////////
        ////////// User Management
        //////////////////////////////
        /// <summary>
        /// Creates a new user account
        /// </summary>
        /// <param name="sessionID">ID of current user's session</param>
        /// <param name="username">New user username</param>
        /// <param name="password">New user password</param>
        /// <param name="firstname">New user's first name</param>
        /// <param name="lastname">New user's last name</param>
        /// <param name="isAdmin">If user has admin permissions</param>
        /// <param name="canApproveAndRejectBroadcasts">If user can approve and reject broadcasts</param>
        /// <returns>True if account created, false otherwise</returns>
        bool CreateUser(int sessionID, string username, string password, string firstname, string lastname, bool isAdmin, bool canApproveAndRejectBroadcasts);

        //Delete user



        //////////////////////////////
        ////////// Broadcasts
        //////////////////////////////
        /// <summary>
        /// Gets the pending broadcasts from the queue
        /// </summary>
        /// <returns>An array of broadcasts waiting for approval/rejection</returns>
        Broadcast[] GetPendingBroadcasts();
        /// <summary>
        /// Gets the approved broadcasts from the queue
        /// </summary>
        /// <returns>An array of broadcasts that were approved</returns>
        Broadcast[] GetApprovedBroadcasts();
        /// <summary>
        /// Gets the rejected broadcasts from the queue
        /// </summary>
        /// <returns>An array of broadcasts that were rejected</returns>
        Broadcast[] GetRejectedBroadcasts();
        /// <summary>
        /// Gets the installed gateways
        /// </summary>
        /// <returns>An array of identifiers for installed gateways</returns>
        string[] GetGateways();
        /// <summary>
        /// Creates and adds a new broadcast to the queue
        /// </summary>
        /// <param name="sessionID">ID of user login session</param>
        /// <param name="gateways">Array of gateway identifiers</param>
        /// <param name="message">Message to broadcast</param>
        /// <returns>True for success, false otherwise</returns>
        bool QueueBroadcast(int sessionID, string[] gateways, string message);
        /// <summary>
        /// Creates and adds a new broadcast to the queue
        /// </summary>
        /// <param name="sessionID">ID of user login session</param>
        /// <param name="gateways">Array of gateway identifiers</param>
        /// <param name="message">Message to broadcast</param>
        /// <param name="original">Original broadcast new broadcast is made from</param>
        /// <returns>True for success, false otherwise</returns>
        bool QueueBroadcast(int sessionID, string[] gateways, string message, Broadcast original);
        /// <summary>
        /// Approves a broadcast from the queue for distribution
        /// </summary>
        /// <param name="sessionID">ID of user login session</param>
        /// <param name="broadcastID">ID of broadcast to approve</param>
        /// <returns>True for success, false otherwise</returns>
        bool ApproveBroadcast(int sessionID, int broadcastID);
        /// <summary>
        /// Rejects a broadcast from the queue for distribution
        /// </summary>
        /// <param name="sessionID">ID of user login session</param>
        /// <param name="broadcastID">ID of broadcast to reject</param>
        /// <returns>True for success, false otherwise</returns>
        bool RejectBroadcast(int sessionID, int broadcastID);



        //////////////////////////////
        ////////// Signs
        //////////////////////////////
        /// <summary>
        /// Gets currently connected signs
        /// </summary>
        /// <returns>An array of currently connected signs</returns>
        Sign[] GetSigns();
        /// <summary>
        /// Sends a clear screen command to a sign
        /// </summary>
        /// <param name="sessionID">ID of user login session</param>
        /// <param name="signID">ID of sign to send clear screen command to</param>
        /// <returns>True for success, false otherwise</returns>
        bool ClearSignScreen(int sessionID, int signID);
        /// <summary>
        /// Sends a message command to a sign
        /// </summary>
        /// <param name="sessionID">ID of user login session</param>
        /// <param name="signID">ID of sign to send message command to</param>
        /// <param name="message">Message to send to sign</param>
        /// <returns>True for success, false otherwise</returns>
        bool SendSignMessage(int sessionID, int signID, string message);


    }
}
