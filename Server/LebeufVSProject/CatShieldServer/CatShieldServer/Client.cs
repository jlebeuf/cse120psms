﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldServer
{
    class Client
    {
        private UserData userData;
        public UserData UserData { get { return userData; } }
        private DateTime lastActionTime;
        public DateTime LastActionTime { get { return lastActionTime; } }
        private int sessionID;
        public int SessionID { get { return sessionID; } }


        public Client(UserData _userData, int _sessionID)
        {
            userData = _userData;
            sessionID = _sessionID;
            lastActionTime = DateTime.Now;
        }

        protected void UpdateLastActionTime()
        {
            lastActionTime = DateTime.Now;
        }

        public DateTime TimeSinceLastAction()
        {
            return new DateTime(DateTime.Now.Ticks - lastActionTime.Ticks);
        }

        public bool TimedOut(TimeSpan timeout)
        {
            long nowTicks = DateTime.Now.Ticks;
            return ((nowTicks - lastActionTime.Ticks) > timeout.Ticks);
        }
    }
}
