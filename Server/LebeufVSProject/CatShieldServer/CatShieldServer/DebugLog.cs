﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldServer
{
    public static class DebugLog
    {

        public delegate void RecieveMessageEventHandler(string message);
        static public event RecieveMessageEventHandler RecieveMessageCallback;


        static public void Write(string message)
        {
            if (RecieveMessageCallback != null)
                RecieveMessageCallback(message);
        }

        static public void Write(string format, object obj1)
        {
            Write(String.Format(format, obj1));
        }

        static public void Write(string format, object obj1, object obj2)
        {
            Write(String.Format(format, obj1, obj2));
        }

        static public void Write(string format, object obj1, object obj2, object obj3)
        {
            Write(String.Format(format, obj1, obj2, obj3));
        }



        static public void WriteLine(string message)
        {
            if (RecieveMessageCallback != null)
                RecieveMessageCallback(message + Environment.NewLine);
        }

        static public void WriteLine(string format, object obj1)
        {
            WriteLine(String.Format(format, obj1) + Environment.NewLine);
        }

        static public void WriteLine(string format, object obj1, object obj2)
        {
            WriteLine(String.Format(format, obj1, obj2) + Environment.NewLine);
        }

        static public void WriteLine(string format, object obj1, object obj2, object obj3)
        {
            WriteLine(String.Format(format, obj1, obj2, obj3) + Environment.NewLine);
        }
    }
}
