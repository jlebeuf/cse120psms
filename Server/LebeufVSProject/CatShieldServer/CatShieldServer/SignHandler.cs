﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace CatShieldServer
{
    class SignHandler
    {
        int port;
        int millisecondListenerPortFailTimeout;
        Thread listenerThread;
        LinkedList<Sign> signs;


        /// <summary>
        /// Creates a new sign handler
        /// </summary>
        /// <param name="_port">Port to listen for signs on</param>
        /// <param name="_millisecondListenerPortFailTimeout">Milliseconds to wait for retry on listener creation fail</param>
        public SignHandler(int _port, int _millisecondListenerPortFailTimeout)
        {
            port = _port;
            millisecondListenerPortFailTimeout = _millisecondListenerPortFailTimeout;

            signs = new LinkedList<Sign>();
            
            listenerThread = new Thread(new ThreadStart(ListenerThreadMethod));
            listenerThread.IsBackground = true;
            listenerThread.Start();
        }


        /// <summary>
        /// Gets an array of known signs
        /// </summary>
        /// <returns>An array of known signs</returns>
        public Sign[] GetSigns()
        {
            LinkedList<Sign> retVal = new LinkedList<Sign>();
            foreach (Sign s in signs)
                if (s.CurrentConnectionState == Sign.ConnectionState.Connected)
                    retVal.AddLast(s);
            return retVal.ToArray<Sign>();
        }

        /// <summary>
        /// Sends a clear screen command to sign of given ID
        /// </summary>
        /// <param name="signID">ID of sign to send command to</param>
        /// <returns>True if sent command, false otherwise (invalid ID, connection closed, etc.)</returns>
        public bool ClearScreen(int signID)
        {
            // If two signs share the same Serial (they shouldn't), command will go to both
            bool status = false;
            foreach (Sign s in signs)
            {
                if (s.CurrentConnectionState == Sign.ConnectionState.Connected && s.SerialNumber == signID)
                {
                    s.SendClearScreen();
                    status = true;
                }
            }

            return status;
        }

        /// <summary>
        /// Sends a message to sign of given ID
        /// </summary>
        /// <param name="signID">ID of sign to send message to</param>
        /// <param name="message">Message to send</param>
        /// <returns>True is message sent, false otherwise (invalid ID, connection closed, etc.)</returns>
        public bool SendMessage(int signID, string message)
        {
            // If two signs share the same Serial (they shouldn't), message will go to both
            bool status = false;
            foreach (Sign s in signs)
            {
                if (s.CurrentConnectionState == Sign.ConnectionState.Connected && s.SerialNumber == signID)
                {
                    s.SendMessage(message);
                    status = true;
                }
            }

            return status;
        }


        private void SignStateChange(Sign sign)
        {
            if (sign.CurrentConnectionState == Sign.ConnectionState.Disconnected)
                signs.Remove(sign);
        }


        /// <summary>
        /// Method for listener thread
        /// </summary>
        private void ListenerThreadMethod()
        {
            TcpListener listener = null;
            bool started = false;
            while (!started)
            {
                try
                {
                    listener = new TcpListener(IPAddress.Any, port);
                    listener.Start();
                    started = true;
                }
                catch (Exception e)
                {
                    DebugLog.WriteLine("Failed to create TcpListener on port {0}", port);
                    DebugLog.WriteLine("Error: {0}", e.Message);
                    DebugLog.WriteLine("Waiting for {0} seconds before trying again", (double)millisecondListenerPortFailTimeout / 1000.0);
                    Thread.Sleep(millisecondListenerPortFailTimeout);
                }
            }

            while (true)
            {
                TcpClient tcpClient = listener.AcceptTcpClient();
                CatShieldServer.Sign newSign = new CatShieldServer.Sign(tcpClient);
                newSign.stateChangeDelegate += new Sign.ConnectionStateChange(SignStateChange);
                signs.AddLast(newSign);
            }
        }
    }
}
