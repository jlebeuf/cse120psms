﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldServer
{
    class TwitterGateway : Gateway
    {
        const int TWEET_LENGTH_LIMIT = 140;

        public TwitterGateway()
            : base("Twitter")
        {
            Tweetinvi.TwitterCredentials.SetCredentials("3160120231-ztfh75XlAGBvwFJvtcnKEOTtzxmFNb7A7HmJrkT",
                                                        "bQHHOqk5vFGJIkrLOsddc6nCvbVlw3YnER3TqXS9OPjOR",
                                                        "cUlMrg3Q7oF30OhYbFpnVQ66e",
                                                        "foou7McypXdp19Fy3DcuAdOhf9WEY2hbVZ3Qwe0bv23imqjIul");
        }

        protected override bool ProcessBroadcast(Broadcast broadcast)
        {
            Console.WriteLine("[{0}] recieved broadcast: {1}", base.name, broadcast.ToString());
            if (broadcast.HasGateway(base.name))
            {
                if (broadcast.GetMessage().Length < 1)
                    Console.WriteLine("Empty message, not pushing broadcast");
                else
                    Console.WriteLine("Pushing Broadcast");
            }
            else
            {
                Console.WriteLine("Not Pushing Broadcast");
                return false;
            }

            string message = broadcast.GetMessage();
            string[] subMessages = new string[1 + (message.Length / TWEET_LENGTH_LIMIT)];
            for (int i = 0; i < subMessages.Length; i++)
            {
                int length = Math.Min(TWEET_LENGTH_LIMIT, message.Length-(i*TWEET_LENGTH_LIMIT));
                subMessages[i] = message.Substring(i*TWEET_LENGTH_LIMIT, length);
            }

            Tweetinvi.Core.Interfaces.ITweet tweet = Tweetinvi.Tweet.CreateTweet(subMessages[0]);
            bool tweeted = Tweetinvi.Tweet.PublishTweet(tweet);
            for (int i = 1; i < subMessages.Length; i++)
            {
                if (!tweeted || tweet == null)
                    return false;

                Tweetinvi.Core.Interfaces.ITweet nextTweet = Tweetinvi.Tweet.CreateTweet(subMessages[i]);
                tweeted = Tweetinvi.Tweet.PublishTweetInReplyTo(nextTweet, tweet);
                tweet = nextTweet;
            }

            return true;
        }
    }
}
