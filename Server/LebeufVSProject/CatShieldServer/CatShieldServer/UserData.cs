﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldServer
{
    public class UserData
    {
        private int userID;
        public int UserID { get { return userID; } }
        private string username;
        public string Username { get { return username; } }
        private string firstName;
        public string FirstName { get { return firstName; } }
        private string lastName;
        public string LastName { get { return lastName; } }
        private bool isAdmin;
        public bool IsAdmin { get { return isAdmin; } }
        private bool canApproveAndRejectBroadcasts;
        public bool CanApproveAndRejectBroadcasts { get { return canApproveAndRejectBroadcasts; } }


        public UserData(int _userID, string _username, string _firstName, string _lastName, bool _isAdmin, bool _canApproveAndRejectBroadcasts)
        {
            userID = _userID;
            username = _username;
            firstName = _firstName;
            lastName = _lastName;
            isAdmin = _isAdmin;
            canApproveAndRejectBroadcasts = _canApproveAndRejectBroadcasts;
        }
    }
}
