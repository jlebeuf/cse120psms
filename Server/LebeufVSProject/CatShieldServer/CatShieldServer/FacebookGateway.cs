﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatShieldServer
{
    class FacebookGateway : Gateway
    {
        public FacebookGateway()
            : base("Facebook")
        {
            
        }

        public FacebookGateway(string name)
            : base(name)
        { }

        protected override bool ProcessBroadcast(Broadcast broadcast)
        {
            Console.WriteLine("[{0}] recieved broadcast: {1}", base.name, broadcast.ToString());
            if (broadcast.HasGateway(base.name))
            {
                if (broadcast.GetMessage().Length < 1)
                    Console.WriteLine("Empty message, not pushing broadcast");
                else
                    Console.WriteLine("Pushing Broadcast");
            }
            else
            {
                Console.WriteLine("Not Pushing Broadcast");
                return false;
            }

            try
            {
                Facebook.FacebookClient client = new Facebook.FacebookClient("110765af81f7ecf555af2161ee857475");
                dynamic result = client.Get("oauth/access_token", new {
                            client_id = "1391423877848678",
                            client_secret = "43c1bdb6b0b9fd3fd0eb4f82590717ab",
                            grant_type = "client_credentials",
                        });
                result = client.Post("me/feed/", new { message = broadcast.GetMessage() });
            } catch (Exception e)
            {
                return false;
            }
    
            return true;
        }
    }
}
