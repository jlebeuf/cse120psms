﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldServer
{
    /// <summary>
    /// Represents a broadcast message
    /// </summary>
    public class Broadcast
    {
        readonly int id;
        readonly string creatorUsername;
        readonly string[] gateways;
        readonly string message;
        readonly Broadcast original;


        /// <summary>
        /// Creates a new broadcast of given message to given gateways
        /// </summary>
        /// <param name="_message">Message to send</param>
        /// <param name="_gateways">Gateways to target</param>
        public Broadcast(int _id, string _creatorUsername, string _message, string[] _gateways)
            : this(_id, _creatorUsername, _message, _gateways, null) { }

        /// <summary>
        /// Creates a new broadcast of given message to given gateways
        /// Notes broadcast originated from
        /// </summary>
        /// <param name="_message">Message to send</param>
        /// <param name="_gateways">Gateways to target</param>
        /// <param name="_original">Original broadcast this is made from</param>
        public Broadcast(int _id, string _creatorUsername, string _message, string[] _gateways, Broadcast _original)
        {
            id = _id;
            creatorUsername = _creatorUsername;
            message = _message;
            gateways = new string[_gateways.Length];
            for (int i = 0; i < gateways.Length; i++)
                gateways[i] = _gateways[i];
            original = _original;
        }


        /// <summary>
        /// Gets a deep copy of target gateways
        /// </summary>
        /// <returns>Target gateways</returns>
        public string[] GetGateways()
        {
            // Return deep copy array, don't let accessor change local
            string[] result = new string[gateways.Length];
            for (int i = 0; i < gateways.Length; i++)
                result[i] = gateways[i];
            return result;
        }

        /// <summary>
        /// Gets ID
        /// </summary>
        /// <returns>Broadcast ID</returns>
        public int GetID()
        {
            return id;
        }

        /// <summary>
        /// Gets username of creator
        /// </summary>
        /// <returns>Username of creator</returns>
        public string GetCreatorUsername()
        {
            return creatorUsername;
        }

        /// <summary>
        /// Gets message
        /// </summary>
        /// <returns>Broadcast message</returns>
        public string GetMessage()
        {
            return message;
        }

        /// <summary>
        /// Checks if given gateway is targetted
        /// </summary>
        /// <param name="gateway">Gateway to check</param>
        /// <returns>True if gateway is targetted, false otherwise</returns>
        public bool HasGateway(string gateway)
        {
            for (int i = 0; i < gateways.Length; i++)
                if (gateways[i] == gateway)
                    return true;
            return false;
        }

        /// <summary>
        /// Gets broadcast this was based on
        /// </summary>
        /// <returns>Original broadcast or null</returns>
        public Broadcast GetOriginalBroadcast()
        {
            return original;
        }

        /// <summary>
        /// Makes string from broadcast
        /// </summary>
        /// <returns>String representing broadcast</returns>
        public override string ToString()
        {
            string result = "[" + id + ", " + creatorUsername + ": {" + message + "} to {";
            for (int i = 0; i < gateways.Length; i++)
            {
                if (i > 0)
                    result += ", ";
                result += gateways[i];
            }
            result += "}";

            if (original != null)
                result += " FROM " + original.ToString();
            
            result += "]";

            return result;
        }
    }
}
