﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace CatShieldServer
{
    /// <summary>
    /// External interface to system
    /// </summary>
    class UserInterface : IUserInterface
    {
        DatabaseHandler databaseHandler;
        ClientHandler clientHandler;
        SignHandler signHandler;
        DistributionHandler distributionHandler;
        BroadcastQueue broadcastQueue;


        public UserInterface(DatabaseHandler _databaseHandler, ClientHandler _clientHandler, SignHandler _signHandler, DistributionHandler _distributionHandler, BroadcastQueue _broadcastQueue)
        {
            databaseHandler = _databaseHandler;
            clientHandler = _clientHandler;
            signHandler = _signHandler;
            distributionHandler = _distributionHandler;
            broadcastQueue = _broadcastQueue;
        }


        //////////////////////////////
        ////////// SESSION MANAGEMENT
        //////////////////////////////
        public int NULL_SESSION_ID { get { return ClientHandler.NULL_SESSION_ID; } }

        public int Login(string username, string password)
        {
            return clientHandler.LoginUser(username, password);
        }

        public bool LoggedIn(int sessionID)
        {
            return clientHandler.LoggedInUser(sessionID);
        }

        public UserData GetUserData(int sessionID)
        {
            return clientHandler.GetUserData(sessionID);
        }

        public bool Logout(int sessionID)
        {
            return clientHandler.LogoutUser(sessionID);
        }


        //////////////////////////////
        ////////// User Management
        //////////////////////////////
        public bool CreateUser(int sessionID, string username, string password, string firstname, string lastname, bool isAdmin, bool canApproveAndRejectBroadcasts)
        {
            UserData userData = clientHandler.GetUserData(sessionID);
            if (userData == null || !userData.IsAdmin)
                return false;
            return databaseHandler.CreateUser(username, firstname, lastname, password, isAdmin, canApproveAndRejectBroadcasts);
        }


        //////////////////////////////
        ////////// Broadcasts
        //////////////////////////////
        public Broadcast[] GetPendingBroadcasts()
        {
            return broadcastQueue.GetPendingBroadcasts();
        }
        public Broadcast[] GetApprovedBroadcasts()
        {
            return broadcastQueue.GetApprovedBroadcasts();
        }
        public Broadcast[] GetRejectedBroadcasts()
        {
            return broadcastQueue.GetRejectedBroadcasts();
        }
        public string[] GetGateways()
        {
            return distributionHandler.GatewaysToStringArray();
        }
        public bool QueueBroadcast(int sessionID, string[] gateways, string message)
        {
            UserData userData = clientHandler.GetUserData(sessionID);
            if (userData == null)
                return false;
            Broadcast broadcast = new Broadcast(databaseHandler.ClaimNextBroadcastID(), userData.Username, message, gateways);
            if (broadcastQueue.QueueBroadcast(broadcast))
                return true;
            return false;
        }
        public bool QueueBroadcast(int sessionID, string[] gateways, string message, Broadcast original)
        {
            UserData userData = clientHandler.GetUserData(sessionID);
            if (userData == null)
                return false;
            Broadcast broadcast = new Broadcast(databaseHandler.ClaimNextBroadcastID(), userData.Username, message, gateways, original);
            if (broadcastQueue.QueueBroadcast(broadcast))
                return true;
            return false;
        }
        public bool ApproveBroadcast(int sessionID, int broadcastID)
        {
            UserData userData = clientHandler.GetUserData(sessionID);
            if (userData == null || !userData.CanApproveAndRejectBroadcasts)
                return false;
            if (broadcastQueue.ApproveBroadcast(broadcastID))
                return true;
            return false;
        }
        public bool RejectBroadcast(int sessionID, int broadcastID)
        {
            UserData userData = clientHandler.GetUserData(sessionID);
            if (userData == null || !userData.CanApproveAndRejectBroadcasts)
                return false;
            if (broadcastQueue.RejectBroadcast(broadcastID))
                return true;
            return false;
        }



        //////////////////////////////
        ////////// Signs
        //////////////////////////////
        public Sign[] GetSigns()
        {
            return signHandler.GetSigns();
        }
        public bool ClearSignScreen(int sessionID, int signID)
        {
            UserData userData = clientHandler.GetUserData(sessionID);
            if (userData == null)
                return false;
            if (signHandler.ClearScreen(signID))
                return true;
            return false;
        }
        public bool SendSignMessage(int sessionID, int signID, string message)
        {
            UserData userData = clientHandler.GetUserData(sessionID);
            if (userData == null)
                return false;
            if (signHandler.SendMessage(signID, message))
                return true;
            return false;
        }
    }
}
