﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldServer
{
    /// <summary>
    /// Links subcomponents together and returns user interface to system
    /// </summary>
    public class Server
    {
        DatabaseHandler databaseHandler;
        DistributionHandler distributionHandler;
        BroadcastQueue broadcastQueue;
        SignHandler signHandler;
        ClientHandler clientHandler;
        UserInterface userInterface;


        public Server(int _signListenerPort, int _signListnerCreationFailureMillisecTimeout)
        {
            databaseHandler = new DatabaseHandler();
            distributionHandler = new DistributionHandler();
            distributionHandler.AddGateway(new TwitterGateway());
            distributionHandler.AddGateway(new FacebookGateway());
            distributionHandler.AddGateway(new EmailGateway(databaseHandler));
            distributionHandler.AddGateway(new SMSGateway(databaseHandler));
            broadcastQueue = new BroadcastQueue(distributionHandler);
            signHandler = new SignHandler(_signListenerPort, _signListnerCreationFailureMillisecTimeout);
            clientHandler = new ClientHandler(databaseHandler, new TimeSpan(0, 15, 0));

            userInterface = new UserInterface(databaseHandler, clientHandler, signHandler, distributionHandler, broadcastQueue);
        }

        public IUserInterface GetUserInterface()
        {
            return userInterface;
        }
    }
}
