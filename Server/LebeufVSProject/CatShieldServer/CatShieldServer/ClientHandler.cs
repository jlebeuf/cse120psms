﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace CatShieldServer
{
    class ClientHandler
    {
        public static readonly int NULL_SESSION_ID = 0;

        class ClientEncapsulator : Client
        {
            public ClientEncapsulator(UserData _userData, int _sessionID) : base(_userData, _sessionID) {}


            new public void UpdateLastActionTime()
            {
                base.UpdateLastActionTime();
            }
        }

        private DatabaseHandler databaseHandler;
        private ReaderWriterLockSlim mapsLock;
        private LinkedList<ClientEncapsulator> clients;
        private TimeSpan loginTimeout;
        private Random rand = new Random(); // Generating session IDs for now


        public ClientHandler(DatabaseHandler _databaseHandler, TimeSpan _loginTimeout)
        {
            databaseHandler = _databaseHandler;
            mapsLock = new ReaderWriterLockSlim();
            clients = new LinkedList<ClientEncapsulator>();
            loginTimeout = _loginTimeout;
        }


        /// <summary>
        /// Adds a client if not already contained
        /// </summary>
        /// <param name="_client">Client to add</param>
        /// <returns>True if client added, false if already contained</returns>
        /// <remarks>Not thread safe</remarks>
        private bool AddClient(ClientEncapsulator _client)
        {
            bool contains = false;

            foreach (Client c in clients)
            {
                if (c == _client)
                {
                    contains = true;
                    break;
                }
            }
            if (!contains)
                clients.AddLast(_client);
            
            return !contains;
        }

        /// <summary>
        /// Removes a client
        /// </summary>
        /// <param name="_client">Client to remove</param>
        /// <returns>True if client removed, false otherwise (not contained)</returns>
        /// <remarks>Not thread safe</remarks>
        private bool RemoveClient(ClientEncapsulator _client)
        {
            return clients.Remove(_client);
        }

        /// <summary>
        /// Removes a client based on base Client
        /// </summary>
        /// <param name="_client">Client to remove</param>
        /// <returns>True if client removed, false otherwise</returns>
        /// <remarks>Not thread safe</remarks>
        private bool RemoveClient(Client _client)
        {
            foreach (ClientEncapsulator c in clients)
            {
                if (c == _client)
                {
                    clients.Remove(c);
                    return true;
                }
            }

            return false;
        }


        public int LoginUser(string username, string password)
        {
            UserData userData = databaseHandler.ValidateAndGetUserData(username, password);
            if (userData == null)
                return NULL_SESSION_ID;

            mapsLock.EnterUpgradeableReadLock();
            foreach (ClientEncapsulator c in clients)
            {
                if (c.UserData.Username == username)
                {
                    mapsLock.ExitUpgradeableReadLock();
                    c.UpdateLastActionTime();
                    return c.SessionID;
                }
            }

            int sessionID = MakeSessionID();
            ClientEncapsulator client = new ClientEncapsulator(userData, sessionID);
            mapsLock.EnterWriteLock();
            clients.AddLast(client);
            mapsLock.ExitWriteLock();
            mapsLock.ExitUpgradeableReadLock();
            return sessionID;
        }


        public bool LogoutUser(int sessionID)
        {
            mapsLock.EnterUpgradeableReadLock();
            foreach (ClientEncapsulator c in clients)
            {
                if (c.SessionID == sessionID)
                {
                    mapsLock.EnterWriteLock();
                    clients.Remove(c);
                    mapsLock.ExitWriteLock();
                    mapsLock.ExitUpgradeableReadLock();
                    return true;
                }
            }
            mapsLock.ExitUpgradeableReadLock();
            return false;
        }

        public bool LoggedInUser(int sessionID)
        {
            return LoggedInUser(sessionID, true);
        }

        public bool LoggedInUser(int sessionID, bool updateLastActionTime)
        {
            mapsLock.EnterUpgradeableReadLock();
            foreach (ClientEncapsulator c in clients)
            {
                if (c.SessionID == sessionID)
                {
                    bool timedOut = c.TimedOut(loginTimeout);
                    if (timedOut)
                    {
                        mapsLock.EnterWriteLock();
                        clients.Remove(c);
                        mapsLock.ExitWriteLock();
                        mapsLock.ExitUpgradeableReadLock();
                        return false;
                    }
                    else
                    {
                        mapsLock.ExitUpgradeableReadLock();
                        if (updateLastActionTime)
                            c.UpdateLastActionTime();
                        return true;
                    }
                }
            }
            mapsLock.ExitUpgradeableReadLock();
            return false;
        }

        public UserData GetUserData(int sessionID)
        {
            return GetUserData(sessionID, true);
        }

        public UserData GetUserData(int sessionID, bool updateLastActionTime)
        {
            mapsLock.EnterUpgradeableReadLock();
            foreach (ClientEncapsulator c in clients)
            {
                if (c.SessionID == sessionID)
                {
                    bool timedOut = c.TimedOut(loginTimeout);
                    if (timedOut)
                    {
                        mapsLock.EnterWriteLock();
                        clients.Remove(c);
                        mapsLock.ExitWriteLock();
                        mapsLock.ExitUpgradeableReadLock();
                        return null;
                    }
                    else
                    {
                        mapsLock.ExitUpgradeableReadLock();
                        if (updateLastActionTime)
                            c.UpdateLastActionTime();
                        return c.UserData;
                    }
                }
            }
            mapsLock.ExitUpgradeableReadLock();
            return null;
        }

        private int MakeSessionID()
        {
            // TODO: Implement as hash
            // Should implement as hash based on timestamp, rand for now
            bool collision = true;
            int r = 0;
            while (collision)
            {
                collision = false;
                r = rand.Next();
                lock (clients)
                {
                    foreach (ClientEncapsulator c in clients)
                    {
                        if (c.SessionID == r)
                        {
                            collision = true;
                            break;
                        }
                    }
                }
            }
            
            return r;
        }
    }
}
