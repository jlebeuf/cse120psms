﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldServer
{
    class BroadcastQueue
    {
        DistributionHandler distributionHandler;
        LinkedList<Broadcast> pendingBroadcasts;
        LinkedList<Broadcast> approvedBroadcasts;
        LinkedList<Broadcast> rejectedBroadcasts;


        public BroadcastQueue(DistributionHandler _distributionHandler)
        {
            distributionHandler = _distributionHandler;
            pendingBroadcasts = new LinkedList<Broadcast>();
            approvedBroadcasts = new LinkedList<Broadcast>();
            rejectedBroadcasts = new LinkedList<Broadcast>();
        }


        public bool QueueBroadcast(Broadcast broadcast)
        {
            lock (pendingBroadcasts)
            {
                foreach (Broadcast b in pendingBroadcasts)
                    if (b == broadcast)
                        return false;
                pendingBroadcasts.AddFirst(broadcast);
                Broadcast o = broadcast.GetOriginalBroadcast();
                if (o != null)
                    pendingBroadcasts.Remove(o);
            }
            return true;
        }

        public Broadcast[] GetPendingBroadcasts()
        {
            return pendingBroadcasts.ToArray<Broadcast>();
        }

        public Broadcast[] GetApprovedBroadcasts()
        {
            return approvedBroadcasts.ToArray<Broadcast>();
        }

        public Broadcast[] GetRejectedBroadcasts()
        {
            return rejectedBroadcasts.ToArray<Broadcast>();
        }

        public bool ApproveBroadcast(int broadcastID)
        {
            bool status = false;
            LinkedList<LinkedListNode<Broadcast>> nodes = new LinkedList<LinkedListNode<Broadcast>>();
            lock (pendingBroadcasts)
            {
                LinkedListNode<Broadcast> node = pendingBroadcasts.First;
                while (node != null)
                {
                    LinkedListNode<Broadcast> next = node.Next;
                    if (node.Value.GetID() == broadcastID)
                    {
                        nodes.AddLast(node);
                        pendingBroadcasts.Remove(node);
                        approvedBroadcasts.AddLast(node);
                        status = true;
                    }
                    node = next;
                }
            }

            foreach (LinkedListNode<Broadcast> node in nodes)
                distributionHandler.PushBroadcast(node.Value);

            return status;
        }

        public bool RejectBroadcast(int broadcastID)
        {
            bool status = false;
            LinkedList<LinkedListNode<Broadcast>> nodes = new LinkedList<LinkedListNode<Broadcast>>();
            lock (pendingBroadcasts)
            {
                LinkedListNode<Broadcast> node = pendingBroadcasts.First;
                while (node != null)
                {
                    LinkedListNode<Broadcast> next = node.Next;
                    if (node.Value.GetID() == broadcastID)
                    {
                        nodes.AddLast(node);
                        pendingBroadcasts.Remove(node);
                        rejectedBroadcasts.AddLast(node);
                        status = true;
                    }
                    node = next;
                }
            }

            // Inform database of rejected broadcasts

            return status;
        }
    }
}
