﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldServer
{
    class TestGateway : Gateway
    {


        public TestGateway()
            : base("TestGateway")
        { }

        public TestGateway(string name)
            : base(name)
        { }

        protected override bool ProcessBroadcast(Broadcast broadcast)
        {
            Console.WriteLine("[{0}] recieved broadcast: {1}", base.name, broadcast.ToString());
            if (broadcast.HasGateway(base.name))
                Console.WriteLine("Pushing Broadcast");
            else
                Console.WriteLine("Not Pushing Broadcast");
            return true;
        }
    }
}
