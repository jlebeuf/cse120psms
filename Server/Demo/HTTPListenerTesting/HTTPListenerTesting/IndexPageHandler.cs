﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HTTPListenerTesting
{
    class IndexPageHandler : PageHandler
    {


        public IndexPageHandler()
            : base("index.html")
        {

        }


        override public bool HandleRequest(HttpListenerContext context, CatShieldServer.IUserInterface coreInterface)
        {
            HttpListenerRequest request = context.Request;
            // Obtain a response object.
            HttpListenerResponse response = context.Response;

            NameValueCollection PostVars = GetPostVars(context);

            // Construct a response. 
            string responseString = "<HTML><BODY>";

            int sessionID = coreInterface.NULL_SESSION_ID;
            if (GetSessionID(context, ref sessionID))
            {
                if (!coreInterface.LoggedIn(sessionID))
                {
                    ClearSessionID(context);
                    sessionID = coreInterface.NULL_SESSION_ID;
                }
            }

            if (sessionID == coreInterface.NULL_SESSION_ID)
                context.Response.Redirect("login.html");

            CatShieldServer.UserData userData = coreInterface.GetUserData(sessionID);
            if (userData == null)
                userData = new CatShieldServer.UserData(-1, "username", "first", "last", false, false);

            if (PostVars["broadcastID"] != null)
            {
                string broadcastIDStr = PostVars["broadcastID"];
                if (PostVars["approve"] != null)
                {
                    int broadcastID;
                    if (Int32.TryParse(broadcastIDStr, out broadcastID))
                    {
                        if (!coreInterface.ApproveBroadcast(sessionID, broadcastID))
                        {
                            responseString += "Failed to approve broadcast ID: " + broadcastID + "<br/>";
                        }
                    } else
                    {
                        responseString += "Failed to parse broadcast ID: " + broadcastIDStr + "<br/>";
                    }
                }
                else if (PostVars["reject"] != null)
                {
                    int broadcastID;
                    if (Int32.TryParse(broadcastIDStr, out broadcastID))
                    {
                        if (!coreInterface.RejectBroadcast(sessionID, broadcastID))
                        {
                            responseString += "Failed to reject broadcast ID: " + broadcastID + "<br/>";
                        }
                    }
                    else
                    {
                        responseString += "Failed to parse broadcast ID: " + broadcastIDStr + "<br/>";
                    }
                }
            }




            responseString += "<style>"
                           + "body {"
                           + "background-color: #3FB7FF;}"
                           + ".elem {"
                           + "position : absolute;overflow : hidden;}"
                           + ".elem .bg, .elem .bd, .elem .fg {"
                           + "position : absolute;left : 0px;top : 0px;width : 100%;height : 100%;border : 0px;overflow : hidden;}"
                           + ".elem .bg img {width : 100%;height : 100%;}"
                           + ".selector{  position: fixed; top: 0;  left: 0; width: 100%;height: 50px;z-index:1; background: #FFFF00; /* some styling */ border-bottom: 1px solid #333; /* some styling */}"
                           + " </style>";


            responseString += "<div class='selector'>"
                            + "<form action='login.html' method='post'> <p align = 'right'> " + userData.Username + " | "
                            + "<input type='submit' name='logout' value='Logout'></p></form>"
                            + "</div>"

                            + "<div id='TextBox_11' class='elem' style='z-index:0; left:350px; top:80px; width:800px; height:80px; font-size: 150%; font-weight: bold; text-decoration: underline; visibility:inherit;'>Please Select Which Option Path You Would Like To Choose From:</div>";

            responseString += "<form action='sign.html' method='post'> <br/>"
                                + "<input type='submit' name='Signs' value='Signs'id = 'emergency_button' class= 'elem' style='z-index:0; left:500px; top:150px;font-weight: bold; '>>" 
                                + "</form>";

            responseString += "<form action='custom.html' method='post'><br/>"
                                + "<input type='submit' name='Custom Options' value='Custom'class= 'elem' style='z-index:0; left:750px; top:150px; font-weight: bold; '>" 
                                + "</form>";


            responseString += "<div id='TextBox_2' class='elem' style='z-index:0; left:450px; top:175px; width:250px; height:30px;visibility:inherit;'>(Less Time, Pre-Written Messages)</div>"
                                + "<div id='TextBox_2' class='elem' style='z-index:0; left:700px; top:175px; width:250px; height:30px; visibility:inherit;'>(More time, Custom Messages)</div>"
                                + "<div id='Table_Caption' class='elem' style='z-index:0; left:625px; top:230px; width:800px; height:80px; font-size: 110%; font-weight: bold; text-decoration: underline; visibility:inherit;'>Queued Messages:</div>";



            //responseString += "Pending Broadcasts:<br/>";

           responseString+= "<div id = 'Table' class = 'elem' style='z-index:0; left:420px; top:250px; width: 500px; height: 300px;'>";
            CatShieldServer.Broadcast[] broadcasts = coreInterface.GetPendingBroadcasts();
            if (!userData.CanApproveAndRejectBroadcasts)
                responseString += "You do not have sufficient permissions to approve/reject broadcasts<br/>";
            responseString += "<table border = '2' style='width:100%'>"
                            + "<tr><th>Message</th><th>Destination</th></tr>";
            for (int i = 0; i < broadcasts.Length; i++)
            {
                string gateways = "";
                string[] gwArr = broadcasts[i].GetGateways();
                for (int j = 0; j < gwArr.Length; j++)
                {
                    if (j > 0)
                        gateways += ", ";
                    gateways += gwArr[j];
                }
                responseString += "<tr><form action='index.html' method='post'><input type='hidden' name='broadcastID' value='" + broadcasts[i].GetID() + "'>"
                                + "<td>" + broadcasts[i].GetMessage() + "</td> <td>" + gateways + "</td>"
                                + "<td><input type='submit' name='approve' value='Approve'></td>"
                                + "<td><input type='submit' name='reject' value='Reject'></td>"
                                + "</form></tr>";
            }
            responseString += "</table>"
                           + "</div>";

            responseString += "<br/><br/>";

            for (int i = 0; i < EmergencyScenario.Scenarios.Length; i++)
            {
                responseString += "<form action='custom.html' method='post'>"
                                + "<input type='hidden' name='prefill' value='"+EmergencyScenario.Scenarios[i].HTTPGetName+"'>"
                                + "<input type='submit' name='submit' value='"+EmergencyScenario.Scenarios[i].Name+"'>"
                                + "</form>";
            }
            responseString += "</div>";



            responseString += " <div id = 'image_box' class= 'elem' style='z-index:0; left:600px; top:400px; width: 400px; height: 500px; '>"
                                 + "<a href='http://s262.photobucket.com/user/guitarprodigy93/media/0_zpsbtw5avlq.jpg.html' target='_blank'><img src='http://i262.photobucket.com/albums/ii87/guitarprodigy93/0_zpsbtw5avlq.jpg' width = 175px; height = 250px; top = 300px; left = 650px; border='0' alt=' photo 0_zpsbtw5avlq.jpg'/></a>"
                                 + "</div>";


            responseString += "</BODY></HTML>";
            
            



            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
            // Get a response stream and write the response to it.
            response.ContentLength64 = buffer.Length;
            System.IO.Stream output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            // You must close the output stream.
            output.Close();

            return true;
        }
    }
}
