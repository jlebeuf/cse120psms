﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HTTPListenerTesting
{
    class EmergencyPageHandlerOld : PageHandler
    {


        public EmergencyPageHandlerOld()
            : base("emergency.html")
        {

        }

        class Options 
        {
            public string name = "";
            public string description = "";
            public Options() {}
            public Options(string _name, string _description)
            {
                name = _name;
                description = _description;
            }
        }

        static string NotificationOption(string Option, NameValueCollection PostVars)
        {
            string responseString = "";
            if (PostVars[Option + "CB"] == null)
            {
                return "";
            }

            responseString += "<label>" + Option + "</label><br/>"
                            + "<textarea name='" + Option + "TB' rows='10' cols='80'>";
            if (PostVars["Emergency"] != null)
            {

                if (PostVars["Emergency"] == "Fire")
                {
                    responseString += "Closed due to Fire!";
                }
                else if (PostVars["Emergency"] == "Weapon")
                {
                    responseString += "Closed due to Weapon on Campus!";
                }
                else if (PostVars["Emergency"] == "SuspiciousObject")
                {
                    responseString += "Closed due to Suspicious Object on Campus!";
                }
            }

            responseString += "</textarea><br/>";

            return responseString;
        }

        static string BuildingOption(string Building, NameValueCollection PostVars) 
        {
            string responseString = "";

            if (Building == "KolliganLibrary")
            {
                responseString += "Kolligan Library";
            }

            return responseString;
        }
 
        override public bool HandleRequest(HttpListenerContext context, CatShieldServer.IUserInterface coreInterface)
        {
            HttpListenerRequest request = context.Request;
            // Obtain a response object.
            HttpListenerResponse response = context.Response;

            NameValueCollection PostVars = GetPostVars(context);

            // Construct a response. 
            string responseString = "<HTML><BODY>";

            int sessionID = coreInterface.NULL_SESSION_ID;
            if (GetSessionID(context, ref sessionID))
            {
                if (!coreInterface.LoggedIn(sessionID))
                {
                    ClearSessionID(context);
                    sessionID = coreInterface.NULL_SESSION_ID;
                }
            }

            if (sessionID == coreInterface.NULL_SESSION_ID)
                context.Response.Redirect("login.html");
            
            ////////////// Fix if statement
            string AllOptions = PostVars["AllOptions"];

            string TextBoxes = PostVars["TextBoxes"];

            Options[] options = new Options[6];
            options[0] = new Options("Email", "Email");
            options[1] = new Options("TextMessage", "Text Message");
            options[2] = new Options("DigitalSignage", "Digital Signage");
            options[3] = new Options("SocialMedia", "Social Media");
            options[4] = new Options("CatShieldSign1", "CatShield Sign #1");
            options[5] = new Options("CatShieldSign2", "CatShield Sign #2");

            Options[] EmergencyOptions = new Options[3];
            EmergencyOptions[0] = new Options("Fire","Fire");
            EmergencyOptions[1] = new Options("Weapon", "Weapon");
            EmergencyOptions[2] = new Options("SuspiciousObject", "Suspicious Object");

            Options[] BuildingOptions = new Options[5];
            BuildingOptions[0] = new Options("KolliganLibrary", "Kolligan Library");
            BuildingOptions[1] = new Options("ClassroomandOfficeBuilding", "Classroom and Office Building");
            BuildingOptions[2] = new Options("ScienceandEngineering1", "Science and Engineering 1");
            BuildingOptions[3] = new Options("ScienceandEngineering2", "Science and Engineering 2");
            BuildingOptions[4] = new Options("SocialSciencesandManagementBuilding", "Social Sciences and Management Building");


            if (TextBoxes == null)
            {
                responseString += "<form action=emergency.html method='post'> <input type='hidden' name='TextBoxes' value='true'> <br/>"
                                + "<p>"
                                + "<legend>Notification Options: </legend>"
                                + "<input type='checkbox' name='AllOptions' value='All'> All Options<br>";

                for (int i = 0; i < options.Length; i++)
                {
                    responseString += "<input type='checkbox' name='" + options[i].name + "CB' value='" + options[i].name + "'> " + options[i].description + "<br>";
                }

                responseString += "</p>"
                                + "<p>"
                                + "<legend> Click on your Emergency: </legend>";

                for (int i = 0; i < EmergencyOptions.Length; i++)
                {
                    responseString += "<input type='radio' name='Emergency' value='" + EmergencyOptions[i].name + "'> " + EmergencyOptions[i].description + "<br>";
                }

                responseString += "</p>"
                                + "<p>"
                                + "<legend> Please click which of the following buildings will be closed: </legend>";

                for (int i = 0; i < BuildingOptions.Length; i++)
                {
                    responseString += "<input type='checkbox' name='" + BuildingOptions[i].name + "CB' value='" + BuildingOptions[i].name + "'> " + BuildingOptions[i].description + "<br>";
                }
              
                 responseString +=  "</p>"
                                 +"<input type='submit' name='Submit' value='Submit'>"
                                 + "</form>";
                
            }
            else {

                responseString += "<form action=index.html method='post'>";

                if (AllOptions != null)
                {
                    responseString += "<label> Email </label> <br>"
                                    + "<textarea name='EmailTextBox' rows='10' cols='80'> Please enter email here</textarea> <br>"
                                    + "<label> Text Message </label> <br>"
                                    + "<textarea name='TextMessageTextBox' rows='10' cols='50'> Please enter text message here</textarea> <br>"
                                    + "<label> Digital Signage </label> <br>"
                                    + "<textarea name='DigitalSignageTextBox' rows='10' cols='80'> Please enter digital signage here</textarea> <br>"
                                    + "<label> Social Media </label> <br>"
                                    + "<textarea name='SocialMediaTextBox' rows='10' cols='60'> Please enter social media message here</textarea> <br>"
                                    + "<label> CatShield Sign #1 </label> <br>"
                                    + "<textarea name='CatShield#1TextBox' rows='10' cols='50'> Please enter CatShield #1 message here</textarea> <br>"
                                    + "<label> CatShield Sign #2 </label> <br>"
                                    + "<textarea name='CatShield#2TextBox' rows='10' cols='50'> Please enter CatShield #2 message here</textarea> <br>";

                }
                else {
                    //If Email is checked then check which prewritten emergency message was selected 
                    for (int i = 0; i < options.Length; i++)
                    {
                        responseString += NotificationOption(options[i].name, PostVars);
                    }
                }

                responseString += "</form>";
            }                    


                responseString += "</BODY></HTML>";
            




            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
            // Get a response stream and write the response to it.
            response.ContentLength64 = buffer.Length;
            System.IO.Stream output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            // You must close the output stream.
            output.Close();

            return true;
        }
    }
}
