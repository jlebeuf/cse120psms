﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HTTPListenerTesting
{
    class SignPageHandler : PageHandler
    {


        public SignPageHandler()
            : base("sign.html")
        {

        }


        override public bool HandleRequest(HttpListenerContext context, CatShieldServer.IUserInterface coreInterface)
        {
            HttpListenerRequest request = context.Request;
            // Obtain a response object.
            HttpListenerResponse response = context.Response;

            NameValueCollection PostVars = GetPostVars(context);

            // Construct a response. 
            string responseString = "<HTML><BODY>";

            int sessionID = coreInterface.NULL_SESSION_ID;
            if (GetSessionID(context, ref sessionID))
            {
                if (!coreInterface.LoggedIn(sessionID))
                {
                    ClearSessionID(context);
                    sessionID = coreInterface.NULL_SESSION_ID;
                    response.Redirect("login.html");
                }
            }
            CatShieldServer.UserData userData = coreInterface.GetUserData(sessionID);
            if (userData == null)
                userData = new CatShieldServer.UserData(-1, "username", "first", "last", false, false);

            responseString += "<style>"
                           + "body {"
                           + "background-color: #3FB7FF;}"
                           + ".elem {"
                           + "position : absolute;overflow : hidden;}"
                           + ".elem .bg, .elem .bd, .elem .fg {"
                           + "position : absolute;left : 0px;top : 0px;width : 100%;height : 100%;border : 0px;overflow : hidden;}"
                           + ".elem .bg img {width : 100%;height : 100%;}"
                           + ".selector{  position: fixed; top: 0;  left: 0; width: 100%;height: 50px;z-index:1; background: #FFFF00; /* some styling */ border-bottom: 1px solid #333; /* some styling */}"
                           + " </style>";


            responseString += "<div class='selector'>"
                            + "<form action='login.html' method='post'> <p align = 'right'> " + userData.Username + " | "
                            + "<input type='submit' name='logout' value='Logout'></p></form>"
                            + "</div>";


            if (PostVars["signID"] != null)
            {
                string signIDStr = PostVars["signID"];
                int signID;
                if (Int32.TryParse(signIDStr, out signID))
                {
                    if (PostVars["ClearScreen"] != null)
                    {
                        coreInterface.ClearSignScreen(sessionID, signID);
                    }
                    else if (PostVars["SendMessage"] != null && PostVars["MessageText"] != null)
                    {
                        string message = PostVars["MessageText"];
                        if (message == null)
                            message = "";
                        message = HttpUtility.UrlDecode(message);
                        coreInterface.SendSignMessage(sessionID, signID, message);
                    }
                }
            }



            responseString += "<div id = 'Table' class = 'elem' style='z-index:0; left:420px; top:250px; width: 500px; height: 300px;'>";
            responseString += "<table border = '2' style='width:100%'>"
                            + "<tr><th>Serial Number</th><th>Display size</th></tr>";
            CatShieldServer.Sign[] signs = coreInterface.GetSigns();
            for (int i = 0; i < signs.Length; i++)
            {
                responseString += "<form action='sign.html' method='post'><input type='hidden' name='signID' value='" + signs[i].SerialNumber + "'><tr>";

                responseString += "<th>";
                responseString += signs[i].SerialNumber;
                responseString += "</th>";

                responseString += "<th>";
                responseString += signs[i].DisplayLines + " lines by " + signs[i].DisplayColumns + " columns";
                responseString += "</th>";

                responseString += "</tr><tr>";

                responseString += "<th>";
                responseString += "<input type='submit' name='SendMessage' value='Send Message'>";
                responseString += "</th>";

                responseString += "<th>";
                responseString += "<input type='text' name='MessageText' value='Message'>";
                responseString += "</th>";

                responseString += "</tr><tr>";

                responseString += "<th>";
                responseString += "<input type='submit' name='ClearScreen' value='Clear Screen'>";
                responseString += "</th>";

                responseString += "</tr></form>";
            }
            

            responseString += "</BODY></HTML>";
            
            



            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
            // Get a response stream and write the response to it.
            response.ContentLength64 = buffer.Length;
            System.IO.Stream output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            // You must close the output stream.
            output.Close();

            return true;
        }
    }
}
