﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HTTPListenerTesting
{
    class EmergencyScenario
    {
        static private int NextID = 0;
        private int id;
        public int ID { get { return id; } }
        private string name;
        public string Name { get { return name; } }
        private string httpGetName;
        public string HTTPGetName { get { return httpGetName; } }
        private string textBoxPrefill;
        public string TextBoxPrefill { get { return textBoxPrefill + "&#10&#13The following buildings are closed:&#10&#13"; } }

        static public readonly EmergencyScenario[] Scenarios = new EmergencyScenario[] {
            new EmergencyScenario("Fire", "fire", "fire:"),
            new EmergencyScenario("Shooter", "shooter", "shooter:")
        };

        public EmergencyScenario(string _name, string _httpGetName, string _textBoxPrefill)
        {
            id = NextID++;
            name = _name;
            httpGetName = _httpGetName;
            textBoxPrefill = _textBoxPrefill;
        }
    }
}
