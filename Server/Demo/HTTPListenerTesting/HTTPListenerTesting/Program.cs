﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace HTTPListenerTesting
{
    class Program
    {
        static void Main(string[] args)
        {
            LinkedList<PageHandler> pageHandlers = new LinkedList<PageHandler>();
            pageHandlers.AddLast(new TestPageHandler());
            pageHandlers.AddLast(new LoginPageHandler());
            pageHandlers.AddLast(new IndexPageHandler());
            pageHandlers.AddLast(new BroadcastsPageHandler());
            pageHandlers.AddLast(new CustomPageHandler());
            pageHandlers.AddLast(new SignPageHandler());
            pageHandlers.AddLast(new NullRootPageHandler());

            CatShieldServer.Server server = new CatShieldServer.Server(26262, 2500);
            CatShieldServer.IUserInterface coreInterface = server.GetUserInterface();

            string[] prefixes = { "http://*/" };
            if (!HttpListener.IsSupported)
            {
                Console.WriteLine("Windows XP SP2 or Server 2003 is required to use the HttpListener class.");
                return;
            }
            // URI prefixes are required, 
            // for example "http://contoso.com:8080/index/".
            if (prefixes == null || prefixes.Length == 0)
                throw new ArgumentException("prefixes");

            // Create a listener.
            HttpListener listener = new HttpListener();
            // Add the prefixes. 
            foreach (string s in prefixes)
            {
                listener.Prefixes.Add(s);
            }
            listener.Start();
            Console.WriteLine("Listening...");

            TestPageHandler testPageHandler = new TestPageHandler();
            string lastRequestURL;
            do
            {
                // Note: The GetContext method blocks while waiting for a request. 
                HttpListenerContext context = listener.GetContext();
                HttpListenerRequest request = context.Request;
                lastRequestURL = request.RawUrl;
                if (lastRequestURL[0] == '/')
                    lastRequestURL = lastRequestURL.Substring(1);

                bool handled = false;
                foreach (PageHandler ph in pageHandlers)
                {
                    if (ph.HandlesLocalPathRequest(lastRequestURL))
                    {
                        ph.HandleRequest(context, coreInterface);
                        handled = true;
                        break;
                    }
                }

                if (!handled)
                    Handle404(context);

            } while (true);
            listener.Stop();
        }

        static string NameValCollToHTML(NameValueCollection collection)
        {
            string result = "";

            string[] keys = collection.AllKeys;
            foreach (string s in keys)
                result += s + ": " + collection[s] + "<br/>";

            return result;
        }

        static void Handle404(HttpListenerContext context)
        {
            HttpListenerRequest request = context.Request;
            // Obtain a response object.
            HttpListenerResponse response = context.Response;

            response.StatusCode = 404;
            response.StatusDescription = "NOT FOUND";
            
            // Construct a response. 
            string responseString = "<HTML><TITLE>404 Error</TITLE><BODY>";
            responseString += request.Url.LocalPath + ": ";

            responseString += "<br/><br/><br/>";

            responseString += "request.Headers:<br/>";
            responseString += NameValCollToHTML(request.Headers);
            responseString += "<br/><br/><br/>";

            responseString += "request.QueryString:<br/>";
            responseString += NameValCollToHTML(request.QueryString);
            responseString += "<br/><br/><br/>";

            responseString += "Post vars:<br/>";
            responseString += NameValCollToHTML(Parser.parsePost(request.InputStream));
            responseString += "<br/>";

            responseString += "</BODY></HTML>";
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
            // Get a response stream and write the response to it.
            response.ContentLength64 = buffer.Length;
            System.IO.Stream output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            // You must close the output stream.
            output.Close();
        }
    }

    public static class Parser
    {
        public static NameValueCollection parsePost(System.IO.Stream inputStream)
        {
            NameValueCollection result = new NameValueCollection();

            StreamReader reader = new StreamReader(inputStream, Encoding.UTF8);
            string data = reader.ReadToEnd();

            foreach (string vars in data.Split('&'))
            {
                string[] pairs = vars.Split('=');
                if (pairs.Length > 1)
                    result.Add(pairs[0], pairs[1]);
            }

            return result;
        }
    }
}
