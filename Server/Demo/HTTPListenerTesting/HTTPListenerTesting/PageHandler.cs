﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace HTTPListenerTesting
{
    abstract class PageHandler
    {
        private readonly string pageFilename;
        public string PageFilename;

        public PageHandler(string _pageFilename)
        {
            pageFilename = _pageFilename;
        }


        public bool HandlesLocalPathRequest(string localPath)
        {
            if (localPath == pageFilename)
                return true;
            return false;
        }

        // Bool status returned in case it may be needed later
        abstract public bool HandleRequest(HttpListenerContext context, CatShieldServer.IUserInterface coreInterface);

        protected static bool GetSessionID(HttpListenerContext context, ref int sessionID)
        {
            Cookie sessionCookie = context.Request.Cookies["sessionID"];
            if (sessionCookie == null || !Int32.TryParse(sessionCookie.Value, out sessionID))
                return false;

            return true;
        }

        protected static void SetSessionID(HttpListenerContext context, int sessionID)
        {
            Cookie sessionCookie = new Cookie("sessionID", sessionID.ToString());
            context.Response.AppendCookie(sessionCookie);
        }

        protected static void ClearSessionID(HttpListenerContext context)
        {
            Cookie sessionCookie = new Cookie("sessionID", "");
            sessionCookie.Expired = true;
            context.Response.AppendCookie(sessionCookie);
        }

        protected static NameValueCollection GetPostVars(HttpListenerContext context)
        {
            return ParseVars(context.Request.InputStream);
        }

        protected static NameValueCollection ParseVars(System.IO.Stream stream)
        {
            StreamReader reader = new StreamReader(stream, Encoding.UTF8);
            string varString = reader.ReadToEnd();
            return ParseVars(varString);
        }

        protected static NameValueCollection ParseVars(string varString)
        {
            NameValueCollection result = new NameValueCollection();

            foreach (string vars in varString.Split('&'))
            {
                string[] pairs = vars.Split('=');
                if (pairs.Length > 1)
                    result.Add(pairs[0], pairs[1]);
            }

            return result;
        }
    }
}
