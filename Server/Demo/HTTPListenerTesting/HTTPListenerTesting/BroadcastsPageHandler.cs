﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HTTPListenerTesting
{
    class BroadcastsPageHandler : PageHandler
    {


        public BroadcastsPageHandler()
            : base("broadcasts.html")
        {

        }


        override public bool HandleRequest(HttpListenerContext context, CatShieldServer.IUserInterface coreInterface)
        {
            HttpListenerRequest request = context.Request;
            // Obtain a response object.
            HttpListenerResponse response = context.Response;

            NameValueCollection PostVars = GetPostVars(context);

            // Construct a response. 
            string responseString = "<HTML><BODY>";

            int sessionID = coreInterface.NULL_SESSION_ID;
            if (GetSessionID(context, ref sessionID))
            {
                if (!coreInterface.LoggedIn(sessionID))
                {
                    ClearSessionID(context);
                    sessionID = coreInterface.NULL_SESSION_ID;
                }
            }

            if (sessionID == coreInterface.NULL_SESSION_ID)
                context.Response.Redirect("login.html");
            if (true == true)
            {
                string[] str = {"Twitter"};
                coreInterface.QueueBroadcast(sessionID, str, "Test Message", null);
            }

            responseString += "Pending Broadcasts:<br/>";
            CatShieldServer.Broadcast[] broadcasts = coreInterface.GetPendingBroadcasts();
            responseString += "<table>";
            for (int i = 0; i < broadcasts.Length; i++)
            {
                responseString += "<tr>";
                responseString += "<td>" + broadcasts[i].GetID() + "</td><td>" + broadcasts[i].GetMessage() + "</td>";
                responseString += "</tr>";
            }
            responseString += "</table>";
            
            
            
            
            responseString += "</BODY></HTML>";


            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
            // Get a response stream and write the response to it.
            response.ContentLength64 = buffer.Length;
            System.IO.Stream output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            // You must close the output stream.
            output.Close();

            return true;
        }
    }
}
