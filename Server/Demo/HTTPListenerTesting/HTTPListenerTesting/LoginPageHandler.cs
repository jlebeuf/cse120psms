﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HTTPListenerTesting
{
    class LoginPageHandler : PageHandler
    {


        public LoginPageHandler()
            : base("login.html")
        {

        }


        override public bool HandleRequest(HttpListenerContext context, CatShieldServer.IUserInterface coreInterface)
        {
            HttpListenerRequest request = context.Request;
            // Obtain a response object.
            HttpListenerResponse response = context.Response;

            NameValueCollection PostVars = GetPostVars(context);

            // Construct a response. 
            string responseString = "<HTML><BODY>";


            responseString += "<style>"
                         + "body {"
                         + "background-color: #3FB7FF;}"
                         + ".elem {"
                         + "position : absolute;overflow : hidden;}"
                         + ".elem .bg, .elem .bd, .elem .fg {"
                         + "position : absolute;left : 0px;top : 0px;width : 100%;height : 100%;border : 0px;overflow : hidden;}"
                         + ".elem .bg img {width : 100%;height : 100%;}"
                         + ".selector{  position: fixed; top: 0;  left: 0; width: 100%;height: 50px;z-index:1; background: #FFFF00; /* some styling */ border-bottom: 1px solid #333; /* some styling */}"
                         + " </style>"
                         + "<div id='TextBox_1' class='elem' style='z-index:0; left:585px; top:100px; width:300px; height:50px; font-weight: bold; text-decoration: underline; font-size: 200%; visibility:inherit;'>CatShield</div>"
                         + "<div id='TextBox_2' class='elem' style='z-index:0; left:525px; top:161px; width:85px; height:30px;font-weight: bold; visibility:inherit;'>Username:</div>"
                         + "<div id='TextBox_3' class='elem' style='z-index:0; left:525px; top:191px; width:85px; height:29px;font-weight: bold; visibility:inherit;'>Password:</div>"
                         + "<div id='Rectangle_1' class='elem' style='z-index:-1; left:500px; top:90px; width:300px; height:165px; background-color:#ffffff;'></div>";




            int sessionID = coreInterface.NULL_SESSION_ID;
            if (GetSessionID(context, ref sessionID))
            {
                if (!coreInterface.LoggedIn(sessionID))
                {
                    ClearSessionID(context);
                    sessionID = coreInterface.NULL_SESSION_ID;
                }
            }

            string logout = PostVars["logout"];
            if (logout != null)
            {
                ClearSessionID(context);
                sessionID = coreInterface.NULL_SESSION_ID;
            }

            string username = PostVars["username"];
            string password = PostVars["password"];

            if (username != null && password != null)
            {
                int sid = coreInterface.Login(username, password);
                if (sid != coreInterface.NULL_SESSION_ID)
                {
                    if (sessionID != coreInterface.NULL_SESSION_ID)
                        coreInterface.Logout(sessionID);
                    sessionID = sid;
                    SetSessionID(context, sessionID);
                    context.Response.Redirect("index.html");
                }
            }

            
            if (sessionID != coreInterface.NULL_SESSION_ID)
            {
                responseString += "Logged in, session ID: " + sessionID + "<br/><br/>";
                responseString += "<form action='login.html' method='post'><br/>"
                                + "<input type='submit' name='logout' value='Logout'></form>";
            } else
            {




                responseString += "<form action='login.html' method='post'><br/>"
                                + "<input type='text' name='username'  class= 'elem' style='z-index:0; left:600px; top:161px; '><br/>"
                                + "<input type='password' name='password'class= 'elem' style='z-index:0; left:600px; top:191px;' ><br/>"
                                + "<input type='submit' name='login' value='Login' id = 'login_button' class= 'elem' style='z-index:0; left:650px; top:219px; '></form>"
                                + "<div id = 'image_box' class= 'elem' style='z-index:0; left:515px; top:260px; width: 400px; height: 500px; '> "
                                + "<a href='http://s262.photobucket.com/user/guitarprodigy93/media/0_zpsbtw5avlq.jpg.html' target='_blank'><img src='http://i262.photobucket.com/albums/ii87/guitarprodigy93/0_zpsbtw5avlq.jpg' width = 275px; height = 350px; top = 280px; left = 650px; border='0' alt=' photo 0_zpsbtw5avlq.jpg'/></a>"
                                + "</div>";
            
            
            }


            

            responseString += "</BODY></HTML>";
            
            



            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
            // Get a response stream and write the response to it.
            response.ContentLength64 = buffer.Length;
            System.IO.Stream output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            // You must close the output stream.
            output.Close();

            return true;
        }
    }
}
