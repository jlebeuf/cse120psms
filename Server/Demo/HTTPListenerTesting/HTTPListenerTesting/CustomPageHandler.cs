﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace HTTPListenerTesting
{
    class CustomPageHandler : PageHandler
    {


        public CustomPageHandler()
            : base("custom.html")
        {

        }

        class Options 
        {
            public string name = "";
            public string description = "";
            public Options() {}
            public Options(string _name, string _description)
            {
                name = _name;
                description = _description;
            }
        }

        static string NotificationOption(string Option, NameValueCollection PostVars)
        {
            string responseString = "";
            if (PostVars[Option + "CB"] == null)
            {
                return "";
            }

            responseString += "<label>" + Option + "</label><br/>"
                            + "<textarea name='" + Option + "TB' rows='10' cols='80'>";
            if (PostVars["Emergency"] != null)
            {

                if (PostVars["Emergency"] == "Fire")
                {
                    return "Closed due to Fire!";
                }
                else if (PostVars["Emergency"] == "Weapon")
                {
                    return "Closed due to Weapon on Campus!";
                }
                else if (PostVars["Emergency"] == "SuspiciousObject")
                {
                    return "Closed due to Suspicious Object on Campus!";
                }
            }

           // responseString += "</textarea><br/>";

            return responseString;
        }

        static string BuildingOption(string Building, NameValueCollection PostVars) 
        {
            string responseString = "";

            if (Building == "KolliganLibrary")
            {
                responseString += "Kolligan Library";
            }

            return responseString;
        }
 
        override public bool HandleRequest(HttpListenerContext context, CatShieldServer.IUserInterface coreInterface)
        {
            HttpListenerRequest request = context.Request;
            // Obtain a response object.
            HttpListenerResponse response = context.Response;

            NameValueCollection PostVars = GetPostVars(context);

            // Construct a response. 
            string responseString = "<HTML><BODY>";

            int sessionID = coreInterface.NULL_SESSION_ID;
            if (GetSessionID(context, ref sessionID))
            {
                if (!coreInterface.LoggedIn(sessionID))
                {
                    ClearSessionID(context);
                    sessionID = coreInterface.NULL_SESSION_ID;
                }
            }

            if (sessionID == coreInterface.NULL_SESSION_ID)
                context.Response.Redirect("login.html");
            
            string AllOptions = PostVars["AllOptions"];

            string MakeBroadcast = PostVars["MakeBroadcast"];

            string[] gateways = coreInterface.GetGateways();


            Options[] options = new Options[gateways.Length];
            for (int i = 0; i < gateways.Length; i++) 
            {
                options[i] = new Options(gateways[i], gateways[i]);
            }


            if (MakeBroadcast == null)
            {
                responseString += "<form action=custom.html method='post'> <input type='hidden' name='MakeBroadcast' value='true'> <br/>"
                                + "<p>"
                                + "<legend>Notification Options: </legend>"
                                + "<input type='checkbox' name='AllOptions' value='All'> All Options<br>";

                for (int i = 0; i < options.Length; i++)
                {
                    responseString += "<input type='checkbox' name='" + options[i].name + "CB' value='" + options[i].name + "'> " + options[i].description + "<br>";
                }

                responseString += "</p>"
                                 + "<label> TextBox </label><br/>"
                                 + "<textarea name='TextBox' rows='10' cols='80'>";
                string prefill = PostVars["prefill"];
                if (prefill != null)
                {
                    for (int i = 0; i < EmergencyScenario.Scenarios.Length; i++)
                    {
                        if (EmergencyScenario.Scenarios[i].HTTPGetName == prefill)
                        {
                            prefill = EmergencyScenario.Scenarios[i].TextBoxPrefill;
                            break;
                        }
                    }
                } else
                {
                    prefill = "";
                }

                responseString += prefill + "</textarea> <br>"
                                 + "<input type='submit' name='Submit' value='Submit'>"
                                 + "</form>";
                
            }
            else
            {
                string[] targetGateways = null;
                if (AllOptions != null)
                    targetGateways = coreInterface.GetGateways();
                else
                {
                    LinkedList<string> targetList = new LinkedList<string>();
                    foreach (string s in gateways)
                    {
                        if (PostVars[s+"CB"] != null)
                        {
                            targetList.AddLast(s);
                        }
                    }
                    targetGateways = targetList.ToArray<string>();
                }

                string message = PostVars["TextBox"];
                if (message == null)
                    message = "";
                message = HttpUtility.UrlDecode(message);

                bool queueSuccess = coreInterface.QueueBroadcast(sessionID, targetGateways, message);
                if (queueSuccess)
                    responseString += "Queue Success<br/>";
                else
                    responseString += "Queue Failure<br/>";

                /*responseString += "Queued Broadcasts<br/>";
                CatShieldServer.Broadcast[] queued = coreInterface.GetPendingBroadcasts();
                foreach (CatShieldServer.Broadcast b in queued)
                {
                    responseString += b.GetMessage() + "<br/>";
                }*/

                response.Redirect("index.html");

               // responseString += "<form action=index.html method='post'>";

                if (AllOptions != null)
                {
                    

                }
                else {
                    //If Email is checked then check which prewritten emergency message was selected 
                    /*
                    for (int i = 0; i < options.Length; i++)
                    {
                        responseString += NotificationOption(options[i].name, PostVars);
                    }
                     */ 
                }
                    
                responseString += "</form>";
            }                    


                responseString += "</BODY></HTML>";
            




            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
            // Get a response stream and write the response to it.
            response.ContentLength64 = buffer.Length;
            System.IO.Stream output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            // You must close the output stream.
            output.Close();

            return true;
        }
    }
}
