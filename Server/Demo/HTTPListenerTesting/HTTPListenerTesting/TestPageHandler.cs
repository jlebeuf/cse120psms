﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;

namespace HTTPListenerTesting
{
    class TestPageHandler : PageHandler
    {


        public TestPageHandler()
            : base("test.html")
        {

        }


        override public bool HandleRequest(HttpListenerContext context, CatShieldServer.IUserInterface coreInterface)
        {
            HttpListenerRequest request = context.Request;
            // Obtain a response object.
            HttpListenerResponse response = context.Response;
            // Construct a response. 
            string responseString = "<HTML><BODY>";
            responseString += request.Url.LocalPath + ": ";
            responseString += "Hello world!<br/>";

            responseString += "request.Headers:<br/>";
            responseString += NameValCollToHTML(request.Headers);
            responseString += "<br/><br/><br/>";

            responseString += "request.QueryString:<br/>";
            responseString += NameValCollToHTML(request.QueryString);
            responseString += "<br/><br/><br/>";

            responseString += "Post:<br/>";
            responseString += NameValCollToHTML(Parser.parsePost(request.InputStream));
            responseString += "<br/><br/><br/>";

            responseString += "<form action=\"index.html\" method=\"post\">"
                            + "<input type=\"text\" name=\"testField\">"
                            + "<input type=\"submit\" name=\"SubmitButton\" value=\"Submit\"></form>";
            responseString += "</BODY></HTML>";
            byte[] buffer = System.Text.Encoding.UTF8.GetBytes(responseString);
            // Get a response stream and write the response to it.
            response.ContentLength64 = buffer.Length;
            System.IO.Stream output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            // You must close the output stream.
            output.Close();

            return true;
        }

        static string NameValCollToHTML(NameValueCollection collection)
        {
            string result = "";

            string[] keys = collection.AllKeys;
            foreach (string s in keys)
                result += s + ": " + collection[s] + "<br/>";

            return result;
        }
    }
}
