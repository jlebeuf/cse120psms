﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace HTTPListenerTesting
{
    class NullRootPageHandler : PageHandler
    {


        public NullRootPageHandler()
            : base("")
        {

        }


        override public bool HandleRequest(HttpListenerContext context, CatShieldServer.IUserInterface coreInterface)
        {
            HttpListenerResponse response = context.Response;
            response.Redirect("index.html");

            byte[] buffer = System.Text.Encoding.UTF8.GetBytes("");
            // Get a response stream and write the response to it.
            response.ContentLength64 = buffer.Length;
            System.IO.Stream output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            // You must close the output stream.
            output.Close();

            return true;
        }
    }
}
