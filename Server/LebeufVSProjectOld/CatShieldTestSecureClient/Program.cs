﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace CatShieldTestSecureClient
{
    class Program
    {
        static TcpClient client = null;
        static SslStream sslStream = null;


        static void Main(string[] args)
        {
            string testMessage = "Test Message";
            char command;
            do
            {
                Console.Write("[c] Connect or [s] Send message or [q] Quit: ");
                command = Char.ToLower(Console.ReadLine()[0]);

                if (command == 'c')
                {
                    Console.WriteLine("Connecting");
                    connect();
                }
                else
                    if (command == 's')
                    {
                        Console.WriteLine("Sending message: " + testMessage);
                        sendMessage(testMessage);
                    }
                    else
                        if (command == 'q')
                        {
                            Console.WriteLine("Exiting");
                            break;
                        }
                        else
                        {
                            Console.WriteLine("Unknown command");
                        }
            } while (command != 'q');

            disconnect();

        }

        static void connect()
        {
            disconnect();

            try
            {
                client = new TcpClient("localhost", 25253);
                sslStream = new SslStream(client.GetStream(), false, new RemoteCertificateValidationCallback(ValidateServerCertificate), null);
                try
                {
                    sslStream.AuthenticateAsClient("localhost");
                }
                catch (AuthenticationException e)
                {
                    Console.WriteLine("Exception: {0}", e.Message);
                    if (e.InnerException != null)
                    {
                        Console.WriteLine("Inner exception: {0}", e.InnerException.Message);
                    }
                    Console.WriteLine("Authentication failed - closing the connection.");
                    client.Close();
                    client = null;
                    sslStream = null;
                    return;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to connect to localhost");
                client = null;
                sslStream = null;
                return;
            }
        }
        static bool ValidateServerCertificate(
              object sender,
              X509Certificate certificate,
              X509Chain chain,
              SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None || sslPolicyErrors == SslPolicyErrors.RemoteCertificateChainErrors)
                return true;

            Console.WriteLine("Certificate error: {0}", sslPolicyErrors);

            // Do not allow this client to communicate with unauthenticated servers. 
            return false;
        }

        static void disconnect()
        {
            if (sslStream != null)
            {
                sslStream.Close();
                sslStream = null;
            }
            if (client != null)
            {
                client = null;
            }
        }

        static void sendMessage(string message)
        {
            if (sslStream != null)
            {
                byte[] str = System.Text.Encoding.ASCII.GetBytes(message.ToCharArray());
                sslStream.Write(str, 0, str.Length);
            }
            else
            {
                Console.WriteLine("Not connected");
            }
        }
    }
}
