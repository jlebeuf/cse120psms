﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace CatShieldServer
{

    /// <summary>
    /// Takes a solicited client connection and authenticates
    ///     or rejects the user.
    /// Secure client connection can be implemented with (so username/pass not sent plaintext):
    /// SSL:
    ///     https://msdn.microsoft.com/en-us/library/system.net.security.sslstream%28v=vs.90%29.aspx
    /// Simple asymmetric encryption:
    ///     Server has public and private keys KPub and KPrv, send KPub to client, client encrypts
    ///     password with KPub and sends E_KPub(password) to server, server decrypts password and
    ///     compares D_KPrv(E_KPub(password)) to stored password
    /// 
    /// Needs some form of database access for account user/pass lookup/verification
    /// </summary>
    class ClientAuthenticator
    {

        /// <summary>
        /// Constructor for ClientAuthenticator, initializes any needed resources
        ///     to authenticate clients
        /// </summary>
        public ClientAuthenticator()
        {

        }


        /// <summary>
        /// Authenticates a solicited client connection
        /// </summary>
        /// <param name="solicitedClient">Connection from potential client</param>
        /// <returns>New Client object if authenticated, null if failed</returns>
        public Client AuthenticateClient(TcpClient solicitedClient)
        {
            return new Client(solicitedClient);
        }


        /// <summary>
        /// Authenticates a solicited client connection through SSL
        /// </summary>
        /// <param name="solicitedClient">Connection from potential SSL client</param>
        /// <returns>New Client object if authenticated, null if failed</returns>
        public Client AuthenticateSecureClient(TcpClient solicitedClient)
        {
            SslStream sslStream = new SslStream(solicitedClient.GetStream());
            try
            {
                X509Certificate serverCertificate = X509Certificate.CreateFromCertFile("CatShieldDevLocal.cert");
                sslStream.AuthenticateAsServer(serverCertificate,
                    false, SslProtocols.Tls, true);
                // Display the properties and settings for the authenticated stream.
                DisplaySecurityLevel(sslStream);
                DisplaySecurityServices(sslStream);
                DisplayCertificateInformation(sslStream);
                DisplayStreamProperties(sslStream);

                // Set timeouts for the read and write to 120 seconds.
                sslStream.ReadTimeout = 300000;
                sslStream.WriteTimeout = 300000;
            }
            catch (AuthenticationException e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
                if (e.InnerException != null)
                {
                    Console.WriteLine("Inner exception: {0}", e.InnerException.Message);
                }
                Console.WriteLine("Authentication failed - closing the connection.");
                sslStream.Close();
                solicitedClient.Close();
                return null;
            }
            return new Client(solicitedClient, sslStream);
        }
        static void DisplaySecurityLevel(SslStream stream)
        {
            Console.WriteLine("Cipher: {0} strength {1}", stream.CipherAlgorithm, stream.CipherStrength);
            Console.WriteLine("Hash: {0} strength {1}", stream.HashAlgorithm, stream.HashStrength);
            Console.WriteLine("Key exchange: {0} strength {1}", stream.KeyExchangeAlgorithm, stream.KeyExchangeStrength);
            Console.WriteLine("Protocol: {0}", stream.SslProtocol);
        }
        static void DisplaySecurityServices(SslStream stream)
        {
            Console.WriteLine("Is authenticated: {0} as server? {1}", stream.IsAuthenticated, stream.IsServer);
            Console.WriteLine("IsSigned: {0}", stream.IsSigned);
            Console.WriteLine("Is Encrypted: {0}", stream.IsEncrypted);
        }
        static void DisplayStreamProperties(SslStream stream)
        {
            Console.WriteLine("Can read: {0}, write {1}", stream.CanRead, stream.CanWrite);
            Console.WriteLine("Can timeout: {0}", stream.CanTimeout);
        }
        static void DisplayCertificateInformation(SslStream stream)
        {
            Console.WriteLine("Certificate revocation list checked: {0}", stream.CheckCertRevocationStatus);

            X509Certificate localCertificate = stream.LocalCertificate;
            if (stream.LocalCertificate != null)
            {
                Console.WriteLine("Local cert was issued to {0} and is valid from {1} until {2}.",
                    localCertificate.Subject,
                    localCertificate.GetEffectiveDateString(),
                    localCertificate.GetExpirationDateString());
            }
            else
            {
                Console.WriteLine("Local certificate is null.");
            }
            // Display the properties of the client's certificate.
            X509Certificate remoteCertificate = stream.RemoteCertificate;
            if (stream.RemoteCertificate != null)
            {
                Console.WriteLine("Remote cert was issued to {0} and is valid from {1} until {2}.",
                    remoteCertificate.Subject,
                    remoteCertificate.GetEffectiveDateString(),
                    remoteCertificate.GetExpirationDateString());
            }
            else
            {
                Console.WriteLine("Remote certificate is null.");
            }
        }
        private static void DisplayUsage()
        {
            Console.WriteLine("To start the server specify:");
            Console.WriteLine("serverSync certificateFile.cer");
            Environment.Exit(1);
        }
    }
}
