﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldServer
{
    /// <summary>
    /// Abstract display
    /// - Gateway for any device/service capable of receiving
    ///     a broadcast message
    /// - Implemented displays do own logic (posting message to
    ///     Facebook/Twitter/etc., transmitting to sign, etc.)
    /// - Depending on display manifestation (Facebook, physical
    ///     sign, etc.) might have always on connection or connect
    ///     only on broadcast.
    /// </summary>
    abstract class Display
    {
        protected string name = "";



        virtual public string GetName()
        {
            return name;
        }

        /// <summary>
        /// Check if display currently connected. Might be always
        ///     on connection to sign, social media or successful
        ///     login to site within last X minutes(?).
        /// </summary>
        /// <returns>Display currently connected</returns>
        virtual public bool isConnected()
        {
            return false;
        }

        /// <summary>
        /// Recieves a message to display, transmits to display manifestation
        /// </summary>
        /// <param name="msg">Message to display</param>
        /// <returns>Success or failure of displaying message</returns>
        virtual public bool DisplayMessage(BroadcastMessage msg)
        {
            return false;
        }
    }
}
