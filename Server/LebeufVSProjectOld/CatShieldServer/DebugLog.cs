﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldServer
{
    static class DebugLog
    {

        public delegate void RecieveMessageEventHandler(string message);
        static public event RecieveMessageEventHandler RecieveMessageCallback;


        static public void Write(string message)
        {
            RecieveMessageCallback(message);
        }

        static public void WriteLine(string message)
        {
            RecieveMessageCallback(message + "\n");
        }
    }
}
