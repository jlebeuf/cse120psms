﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldServer
{

    /// <summary>
    /// Static handlers for database, provides interface for queries
    /// </summary>
    static class DatabaseHandler
    {

        /// <summary>
        /// Checks if password in database for given user matches given password
        /// </summary>
        /// <param name="username">Username of account</param>
        /// <param name="password">Password of account to verify</param>
        /// <returns></returns>
        public static bool ComparePassword(string username, string password)
        {
            return false;
        }
    }
}
