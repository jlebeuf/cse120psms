﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace CatShieldServer
{

    /// <summary>
    /// Main controller for server
    /// - Accepts connections from clients, verifies login credentials
    /// - Maintains list of connected clients?
    /// - Keeps list of displays
    /// - On receiving client message verifies client's permissions,
    ///     distributes message to displays selected by client
    /// </summary>
    class MainController
    {
        LinkedList<Client> connectedClients; // NOT THREAD SAFE, always use connectedClientsLock before accessing
        object connectedClientsLock = new object();
        ClientAuthenticator clientAuthenticator; // Should be able to access without lock
        LinkedList<Display> displays; // Not thread safe, use displaysLock to access
        object displaysLock = new object();

        /// <summary>
        /// 
        /// </summary>
        public MainController()
        {
            connectedClients = new LinkedList<Client>();
            displays = new LinkedList<Display>();
            clientAuthenticator = new ClientAuthenticator();
            Thread clientListenerThread = new Thread(new ThreadStart(ClientListenerThread));
            clientListenerThread.IsBackground = true;
            clientListenerThread.Start();
            Thread secureClientListenerThread = new Thread(new ThreadStart(SecureClientListenerThread));
            secureClientListenerThread.IsBackground = true;
            secureClientListenerThread.Start();
            Thread displayListenerThread = new Thread(new ThreadStart(DisplayListenerThread));
            displayListenerThread.IsBackground = true;
            displayListenerThread.Start();
        }


        /// <summary>
        /// Example of creating a new 'Client' object (after
        ///     one connects) and assigning callback function
        ///     to execute when client sends a broadcast
        ///     message
        /// </summary>
        private void testMakeClient()
        {
            Client client = new Client(null);
            client.ReceiveMessageCallback += new Client.RecieveMessageEventHandler(RecieveClientMessage);
            client.DisconnectCallback += new Client.DisconnectEventHandler(DisconnectClient);
        }



        public void RegisterClient(Client newClient)
        {
            DebugLog.WriteLine("Registering new client: '" + newClient.GetUsername() + "'");
            lock (connectedClientsLock)
            {
                connectedClients.AddLast(newClient);
                newClient.ReceiveMessageCallback += new Client.RecieveMessageEventHandler(RecieveClientMessage);
                newClient.DisconnectCallback += new Client.DisconnectEventHandler(DisconnectClient);
            }
        }

        public void RegisterDisplay(Display newDisplay)
        {
            DebugLog.WriteLine("Registering new display: '" + newDisplay.GetName() + "'");
            lock (displaysLock)
            {
                displays.AddLast(newDisplay);
            }
        }


        /// <summary>
        /// Processes a broadcast message from a client
        /// </summary>
        /// <param name="message">Message from client to broadcast</param>
        private void RecieveClientMessage(BroadcastMessage message)
        {
            DebugLog.WriteLine("Received message from client: '" + message.GetOriginatingClient().GetUsername() + "' -> '" + message.GetSafeMessageString() + "'");
            foreach (Display dsp in displays)
                dsp.DisplayMessage(message);
        }


        /// <summary>
        /// Called when client disconnects
        /// </summary>
        /// <param name="client">Disconnecting client</param>
        private void DisconnectClient(Client client)
        {
            DebugLog.WriteLine("Disconnecting client: '" + client.GetUsername() + "'");
            lock (connectedClientsLock)
            {
                connectedClients.Remove(client);
            }
        }


        /// <summary>
        /// Called when disconnecting display
        /// </summary>
        /// <param name="display">Disconnecting display</param>
        private void DisconnectDisplay(Display display)
        {
            DebugLog.WriteLine("Disconnecting display: '" + display.GetName() + "'");
            lock (displaysLock)
            {
                displays.Remove(display);
            }
        }


        /// <summary>
        /// Listens for TCP connections on a port, runs ClientAuthenticator on connection
        ///     when one accepted, saves returned Client on success
        /// </summary>
        private void ClientListenerThread()
        {
            TcpListener listener = null;
            bool connected = false;
            while (!connected)
            {
                try
                {
                    listener = new TcpListener(IPAddress.Any, 25252);
                    listener.Start();
                    connected = true;
                } catch(Exception e)
                {

                }
            }

            while (true)
            {
                TcpClient potentialClient = listener.AcceptTcpClient();
                Client newClient = clientAuthenticator.AuthenticateClient(potentialClient);
                if (newClient != null)
                {
                    RegisterClient(newClient);
                }
                else
                    potentialClient.Close();
            }
        }


        /// <summary>
        /// Listens for TCP SSL connections on a port, runs ClientAuthenticator on connection
        ///     when one accepted, saves returned Client on success
        /// </summary>
        private void SecureClientListenerThread()
        {
            TcpListener listener = null;
            bool connected = false;
            while (!connected)
            {
                try
                {
                    listener = new TcpListener(IPAddress.Any, 25253);
                    listener.Start();
                    connected = true;
                }
                catch (Exception e)
                {

                }
            }

            while (true)
            {
                TcpClient potentialClient = listener.AcceptTcpClient();
                Client newClient = clientAuthenticator.AuthenticateSecureClient(potentialClient);
                if (newClient != null)
                {
                    RegisterClient(newClient);
                }
                else
                    potentialClient.Close();
            }
        }


        /// <summary>
        /// Listens for TCP connections on a port
        /// </summary>
        private void DisplayListenerThread()
        {
            TcpListener listener = null;
            bool connected = false;
            while (!connected)
            {
                try
                {
                    listener = new TcpListener(IPAddress.Any, 26262);
                    listener.Start();
                    connected = true;
                }
                catch (Exception e)
                {

                }
            }

            while (true)
            {
                TcpClient tcpClient = listener.AcceptTcpClient();
                NetworkedDisplay newDisplay = new NetworkedDisplay(tcpClient);
                newDisplay.DisconnectCallback += new NetworkedDisplay.DisconnectEventHandler(DisconnectDisplay);
                RegisterDisplay(newDisplay);
            }
        }
    }
}
