﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldServer
{
    /// <summary>
    /// Message to broadcast, may be just a string or
    ///     could contain extra information if needed later
    ///     (such as formating?)
    /// </summary>
    class BroadcastMessage
    {
        public readonly string rawMessage = "";
        // private listOfDisplaysToBroadcastTo
        public readonly Client originatingClient;


        public BroadcastMessage(string message, Client originator)
        {
            this.rawMessage = message;
            this.originatingClient = originator;
        }

        /// <summary>
        /// Getter for message as a string, should NOT contain
        ///     special characters (non alphanumeric/punctuation
        ///     characters such as single character face, heart,
        ///     etc.). Not all displays can handle them (roadside
        ///     sign).
        /// </summary>
        /// <returns>Message as reduced character set string</returns>
        public string GetSafeMessageString()
        {
            return rawMessage;
        }


        // public listOfDisplaysToBroadcastTo GetBroadcastTargets() {}


        /// <summary>
        /// Getter for originating client who message is coming from
        /// </summary>
        /// <returns>The client sending the message</returns>
        public Client GetOriginatingClient()
        {
            return originatingClient;
        }
    }
}
