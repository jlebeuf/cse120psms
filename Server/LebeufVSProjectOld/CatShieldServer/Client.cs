﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Security;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace CatShieldServer
{

    /// <summary>
    /// Client
    /// - Represents a connection from a client
    /// - On receiving message from physical client informs
    ///     parent MainController
    /// </summary>
    class Client
    {
        public delegate void RecieveMessageEventHandler(BroadcastMessage message);
        public event RecieveMessageEventHandler ReceiveMessageCallback;
        public delegate void DisconnectEventHandler(Client client);
        public event DisconnectEventHandler DisconnectCallback;

        private TcpClient tcpClient;
        private Stream clientStream;
        private string username;
        private string fullname;


        public Client(TcpClient tcpClient)
        {
            this.tcpClient = tcpClient;
            if (tcpClient == null)
                this.clientStream = null;
            else
                this.clientStream = tcpClient.GetStream();

            username = this.GetHashCode().ToString();
            fullname = this.GetHashCode().ToString();


            Thread clientListenerThread = new Thread(new ThreadStart(ListeningThread));
            clientListenerThread.IsBackground = true;
            clientListenerThread.Start();
        }

        public Client(TcpClient tcpClient, SslStream sslStream)
        {
            this.tcpClient = tcpClient;
            if (tcpClient == null)
                this.clientStream = null;
            else
                this.clientStream = sslStream;

            username = this.GetHashCode().ToString();
            fullname = this.GetHashCode().ToString();


            Thread clientListenerThread = new Thread(new ThreadStart(ListeningThread));
            clientListenerThread.IsBackground = true;
            clientListenerThread.Start();
        }


        public string GetUsername()
        {
            return username;
        }

        public string GetFullname()
        {
            return fullname;
        }



        private void ListeningThread()
        {
            byte[] tmp = new byte[1];
            while (true)
            {
                // .Receive() is peeking at the socket buffer to check if the underlying socket is still connected
                // http://stackoverflow.com/questions/3782471/disconnecting-tcpclient-and-seeing-that-on-the-other-side
                if (!tcpClient.Connected || clientStream == null)
                {
                    SignalDisconnect();
                    return;
                }
                else
                try
                {
                    if (tcpClient.Client.Receive(tmp, SocketFlags.Peek) == 0)
                    {
                        SignalDisconnect();
                        return;
                    }
                }
                catch (Exception e)
                {
                    SignalDisconnect();
                    return;
                }

                byte[] recvBuffer = new byte[1024];
                int bytesRead;
                try
                {
                    bytesRead = clientStream.Read(recvBuffer, 0, 1024);
                } catch (Exception e)
                {
                    continue;
                }
                if (bytesRead > 0)
                {
                    string message = System.Text.Encoding.ASCII.GetString(recvBuffer, 0, bytesRead);
                    ReceiveMessage(new BroadcastMessage(message, this));
                }
            }
        }


        /// <summary>
        /// Forwards broadcast message received from physical display
        ///     to main controller
        /// Called internally by whatever is listening for client messages
        /// </summary>
        /// <param name="message">Message to broadcase</param>
        private void ReceiveMessage(BroadcastMessage message)
        {
            if (ReceiveMessageCallback != null)
                ReceiveMessageCallback(message);
            else
                DebugLog.WriteLine("Client message received with no callback set");
        }


        /// <summary>
        /// Notifies that client disconnected
        /// </summary>
        private void SignalDisconnect()
        {
            if (DisconnectCallback != null)
                DisconnectCallback(this);
            else
                DebugLog.WriteLine("Client disconnect received with no callback set");
        }
    }
}
