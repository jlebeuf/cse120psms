﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace CatShieldServer
{
    public partial class DebugLogForm : Form
    {
        private string newText = "";
        private object newTextLock = new object();

        public DebugLogForm()
        {
            InitializeComponent();

            DebugLog.RecieveMessageCallback += new DebugLog.RecieveMessageEventHandler(AppendLog);
            Thread appendThread = new Thread(new ThreadStart(TextAppendThread));
            appendThread.Start();
        }


        private void AppendLog(string message)
        {
            lock (newTextLock)
            {
                newText += message;
            }
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(AppendLog), message);
                return;
            }
            textBox1.AppendText(message);
        }

        private void TextAppendThread()
        {
            string text;
            lock (newTextLock)
            {
                text = newText;
                newText = "";
            }

            textBox1.AppendText(text);
        }
    }
}
