﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CatShieldServer
{
    public partial class MainForm : Form
    {
        private MainController mainController;
        private DebugLogForm debugLogForm;


        public MainForm()
        {
            InitializeComponent();
            mainController = new MainController();
            debugLogForm = new DebugLogForm();
            debugLogForm.Show();
            //debugLogForm.Hide();

            Display testDisplay1 = new TestDisplay("Test Display 1");
            Display testDisplay2 = new TestDisplay("Test Display 2");
            mainController.RegisterDisplay(testDisplay1);
            mainController.RegisterDisplay(testDisplay2);
        }
    }
}
