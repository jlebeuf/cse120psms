﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace CatShieldServer
{
    class NetworkedDisplay : Display
    {
        TcpClient client = null;
        NetworkStream clientStream = null;
        public delegate void DisconnectEventHandler(Display client);
        public event DisconnectEventHandler DisconnectCallback;


        public NetworkedDisplay(TcpClient client)
        {
            this.client = client;
            if (client != null)
                this.clientStream = client.GetStream();

            name = this.GetHashCode().ToString();
            

            Thread clientListenerThread = new Thread(new ThreadStart(ListenerThread));
            clientListenerThread.IsBackground = true;
            clientListenerThread.Start();
        }



        override public bool DisplayMessage(BroadcastMessage message)
        {
            if (clientStream != null)
            {
                byte[] str = System.Text.Encoding.ASCII.GetBytes(message.GetSafeMessageString().ToCharArray());
                try
                {
                    clientStream.Write(str, 0, str.Length);
                    DebugLog.WriteLine("Sent message to NetworkedDisplay stream");
                } catch (Exception e)
                {
                    DebugLog.WriteLine("Exception occured trying to write to NetworkedDisplay stream");
                }
            }
            else
            {
                DebugLog.WriteLine("NetworkedDisplay recieved message but is not connected");
                return false;
            }

            return true;
        }



        private void ListenerThread()
        {
            byte[] tmp = new byte[1];
            while (true)
            {
                // .Receive() is peeking at the socket buffer to check if the underlying socket is still connected
                // http://stackoverflow.com/questions/3782471/disconnecting-tcpclient-and-seeing-that-on-the-other-side
                if (!client.Connected || clientStream == null)
                {
                    SignalDisconnect();
                    return;
                }
                else
                try
                {
                    if (client.Client.Receive(tmp, SocketFlags.Peek) == 0)
                    {
                        SignalDisconnect();
                        return;
                    }
                }
                catch (Exception e)
                {
                    SignalDisconnect();
                    return;
                }
            }
        }


        /// <summary>
        /// Notifies that display disconnected
        /// </summary>
        private void SignalDisconnect()
        {
            if (DisconnectCallback != null)
                DisconnectCallback(this);
            else
                DebugLog.WriteLine("Display disconnect received with no callback set");
        }
    }
}
