﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace CatShieldServer
{
    class TestDisplay : Display
    {


        public TestDisplay()
        {
            name = this.GetHashCode().ToString();
        }

        public TestDisplay(string name)
        {
            this.name = name;
        }


        override public bool DisplayMessage(BroadcastMessage message)
        {
            DebugLog.WriteLine(name + ": " + message.GetSafeMessageString());

            return true;
        }
    }
}
