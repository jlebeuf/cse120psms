﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace CatShieldTestClient
{
    class Program
    {
        static TcpClient client = null;
        static NetworkStream clientStream = null;


        static void Main(string[] args)
        {
            string testMessage = "Test Message";
            char command;
            do
            {
                Console.Write("[c] Connect or [s] Send message or [q] Quit: ");
                command = Char.ToLower(Console.ReadLine()[0]);

                if (command == 'c')
                {
                    Console.WriteLine("Connecting");
                    connect();
                } else
                if (command == 's')
                {
                    Console.WriteLine("Sending message: " + testMessage);
                    sendMessage(testMessage);
                } else
                if (command == 'q')
                {
                    Console.WriteLine("Exiting");
                    break;
                } else
                {
                    Console.WriteLine("Unknown command");
                }
            } while (command != 'q');

            disconnect();

        }

        static void connect()
        {
            disconnect();

            try
            {
                client = new TcpClient("localhost", 25252);
            } catch (Exception e)
            {
                Console.WriteLine("Failed to connect to localhost");
                return;
            }
            clientStream = client.GetStream();
        }

        static void disconnect()
        {
            if (clientStream != null)
            {
                clientStream.Close();
                clientStream = null;
            }
            if (client != null)
            {
                client.Close();
                client = null;
            }
        }

        static void sendMessage(string message)
        {
            if (clientStream != null)
            {
                byte[] str = System.Text.Encoding.ASCII.GetBytes(message.ToCharArray());
                clientStream.Write(str, 0, str.Length);
            } else
            {
                Console.WriteLine("Not connected");
            }
        }
    }
}
