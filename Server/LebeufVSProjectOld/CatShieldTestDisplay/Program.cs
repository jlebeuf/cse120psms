﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace CatShieldTestDisplay
{
    class Program
    {
        static TcpClient client = null;
        static NetworkStream clientStream = null;


        static void Main(string[] args)
        {
            char command;
            Thread displayListenerThread = new Thread(new ThreadStart(ListeningThread));
            displayListenerThread.IsBackground = true;
            displayListenerThread.Start();
            do
            {
                Console.WriteLine("[c] Connect or [q] Quit: ");
                command = Char.ToLower(Console.ReadLine()[0]);

                if (command == 'c')
                {
                    Console.WriteLine("Connecting");
                    connect();
                }
                else
                if (command == 'q')
                {
                    Console.WriteLine("Exiting");
                    break;
                }
                else
                {
                    Console.WriteLine("Unknown command");
                }
            } while (command != 'q');

            disconnect();
            displayListenerThread.Abort();

        }

        static bool connect()
        {
            disconnect();

            try
            {
                client = new TcpClient("192.168.0.104", 26262);
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to connect to localhost");
                return false;
            }
            clientStream = client.GetStream();
            return true;
        }

        static void disconnect()
        {
            if (clientStream != null)
            {
                clientStream.Close();
                clientStream = null;
            }
            if (client != null)
            {
                client.Close();
                client = null;
            }
        }

        static void ListeningThread()
        {
            byte[] tmp = new byte[1];
            while (true)
            {
                // .Receive() is peeking at the socket buffer to check if the underlying socket is still connected
                // http://stackoverflow.com/questions/3782471/disconnecting-tcpclient-and-seeing-that-on-the-other-side
                if (client == null || !client.Connected || clientStream == null)
                {
                    continue;
                } else
                try
                {
                    if (client.Client.Receive(tmp, SocketFlags.Peek) == 0)
                    {
                        Console.WriteLine("Disconnected from server");
                        disconnect();
                        continue;
                    }
                } catch (Exception e)
                {
                    disconnect();
                    continue;
                }

                byte[] recvBuffer = new byte[1024];
                int bytesRead;
                try
                {
                    bytesRead = clientStream.Read(recvBuffer, 0, 1024);
                }
                catch (Exception e)
                {
                    continue;
                }
                if (bytesRead > 0)
                {
                    //string message = System.Text.Encoding.ASCII.GetString(recvBuffer, 0, bytesRead);
                    Console.Write("Recieved message: ");
                    for (int i = 0; i < bytesRead; i++)
                        Console.Write("{0:X} ", recvBuffer[i]);
                    Console.WriteLine();
                }
            }
        }
    }
}
