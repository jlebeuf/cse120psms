Sometimes there are issues when one system opens a Visual Studio project made on another system. This folder contains the VS project on my (Justin's) system. Copy entire CatShieldServer folder to your own directory to try opening as a VS project. VS will be needed to compile the project.

Recommended steps:
- Copy newest CatShieldServer folder to own clearly marked directory (eg. LastnameVSProject).
- Open in VS and follow any prompts about versioning mismatch or whatnot, let it repair/update/etc. the project.
- AVOID opening or modifying files/projects in others' directory, copy from them. Modifying them will modify them for the other user as well.

