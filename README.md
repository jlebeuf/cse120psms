CSE120 Public Safety Messaging System

Written for CSE-120, CatShield is a centralized broadcasting prototype system that combines email, SMS, Twitter, and Facebook broadcasting with centralized control of custom hardware signs. This project is NOT licensed for any use outside of the UC Merced Department of Public Safety.

Credentials have been changed prior to making repository public. Any attempts at gaining unauthorized access to any services will be prosecuted at the discretion of UC Merced.
