﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;

namespace CatShieldDesktopClient
{
    class Client
    {
        private ConnectionState _connectionState;
        public ConnectionState connectionState
        {
            get { return _connectionState; }
        }
        private TcpClient tcpClient;
        private NetworkStream clientStream;

        public Client()
        {
            _connectionState = ConnectionState.Disconnected;
            tcpClient = null;
            clientStream = null;
        }


        /// <summary>
        /// Initiates connection to server
        /// </summary>
        /// <returns>Connection success or failure</returns>
        public bool Connect()
        {
            if (_connectionState == ConnectionState.Connected || _connectionState == ConnectionState.Connecting)
                return false;

            try
            {
                tcpClient = new TcpClient("localhost", 25252);
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to connect to localhost");
                return false;
            }

            clientStream = tcpClient.GetStream();
            _connectionState = ConnectionState.Connected;
            return true;
        }

        /// <summary>
        /// Disconnects from server
        /// </summary>
        /// <returns>Disconnection success or failure</returns>
        public bool Disconnect()
        {
            if (_connectionState == ConnectionState.Disconnected || _connectionState == ConnectionState.Disconnecting)
                return false;

            if (clientStream != null)
            {
                clientStream.Close();
                clientStream = null;
            }
            if (tcpClient != null)
            {
                tcpClient.Close();
                tcpClient = null;
            }
            _connectionState = ConnectionState.Disconnected;
            return true;
        }


        /// <summary>
        /// Send a test message to server
        /// </summary>
        /// <param name="message">Message to send</param>
        public void sendTestMessage(string message)
        {
            if (clientStream != null)
            {
                byte[] str = System.Text.Encoding.ASCII.GetBytes(message.ToCharArray());
                clientStream.Write(str, 0, str.Length);
            }
            else
            {
                Console.WriteLine("Not connected");
            }
        }
    }
}
