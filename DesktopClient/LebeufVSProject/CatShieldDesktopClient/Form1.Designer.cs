﻿namespace CatShieldDesktopClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.connectBTN = new System.Windows.Forms.Button();
            this.disconnectBTN = new System.Windows.Forms.Button();
            this.sendMessageBTN = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // connectBTN
            // 
            this.connectBTN.Location = new System.Drawing.Point(13, 13);
            this.connectBTN.Name = "connectBTN";
            this.connectBTN.Size = new System.Drawing.Size(259, 23);
            this.connectBTN.TabIndex = 0;
            this.connectBTN.Text = "Connect";
            this.connectBTN.UseVisualStyleBackColor = true;
            this.connectBTN.Click += new System.EventHandler(this.connectBTN_Click);
            // 
            // disconnectBTN
            // 
            this.disconnectBTN.Location = new System.Drawing.Point(13, 43);
            this.disconnectBTN.Name = "disconnectBTN";
            this.disconnectBTN.Size = new System.Drawing.Size(259, 23);
            this.disconnectBTN.TabIndex = 1;
            this.disconnectBTN.Text = "Disconnect";
            this.disconnectBTN.UseVisualStyleBackColor = true;
            this.disconnectBTN.Click += new System.EventHandler(this.disconnectBTN_Click);
            // 
            // sendMessageBTN
            // 
            this.sendMessageBTN.Location = new System.Drawing.Point(13, 73);
            this.sendMessageBTN.Name = "sendMessageBTN";
            this.sendMessageBTN.Size = new System.Drawing.Size(259, 23);
            this.sendMessageBTN.TabIndex = 2;
            this.sendMessageBTN.Text = "SendMessage";
            this.sendMessageBTN.UseVisualStyleBackColor = true;
            this.sendMessageBTN.Click += new System.EventHandler(this.sendMessageBTN_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.sendMessageBTN);
            this.Controls.Add(this.disconnectBTN);
            this.Controls.Add(this.connectBTN);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button connectBTN;
        private System.Windows.Forms.Button disconnectBTN;
        private System.Windows.Forms.Button sendMessageBTN;
    }
}

