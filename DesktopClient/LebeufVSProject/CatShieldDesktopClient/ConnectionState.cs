﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CatShieldDesktopClient
{
    enum ConnectionState
    {
        Disconnected,
        Connecting,
        Connected,
        Disconnecting
    }
}
