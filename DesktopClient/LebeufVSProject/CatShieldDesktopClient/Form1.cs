﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CatShieldDesktopClient
{
    public partial class Form1 : Form
    {
        Client client;

        public Form1()
        {
            InitializeComponent();
            client = new Client();
        }

        private void connectBTN_Click(object sender, EventArgs e)
        {
            client.Connect();
        }

        private void disconnectBTN_Click(object sender, EventArgs e)
        {
            client.Disconnect();
        }

        private void sendMessageBTN_Click(object sender, EventArgs e)
        {
            client.sendTestMessage("Dear Server,\nTest.\n\nCordially yours,\nclient");
        }
    }
}
