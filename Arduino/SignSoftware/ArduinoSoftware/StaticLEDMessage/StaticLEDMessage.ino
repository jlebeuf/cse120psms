
/*
  Web client
 
 This sketch connects to a website (http://www.google.com)
 using a WiFi shield.
 
 This example is written for a network using WPA encryption. For 
 WEP or WPA, change the Wifi.begin() call accordingly.
 
 This example is written for a network using WPA encryption. For 
 WEP or WPA, change the Wifi.begin() call accordingly.
 
 Circuit:
 * WiFi shield attached
 
 created 13 July 2010
 by dlf (Metodo2 srl)
 modified 31 May 2012
 by Tom Igoe
 */


#include <SPI.h>
#include <Adafruit_CharacterOLED.h>
#include "HT1632.h"
#define READ_BUFFER_SIZE 128

Adafruit_CharacterOLED lcd(OLED_V2, 14, 15, 16, 17, 18, 19, 2);

#define LED_DATA 3
#define LED_WR 5
#define LED_CS 6
#define LED_CS2 8
HT1632LEDMatrix matrix = HT1632LEDMatrix(LED_DATA, LED_WR, LED_CS, LED_CS2);

void setup() {
  //Initialize serial and wait for port to open:
  Serial.begin(9600); 
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  
  Serial.println("Starting LCD");
  lcd.begin(16, 2);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Starting LED");
  matrix.begin(HT1632_COMMON_16NMOS);
  matrix.fillScreen();
  delay(1000);
  matrix.clearScreen();
  lcd.clear();
  lcd.setCursor(0, 0);
  delay(1000);
  
  matrix.clearScreen();
  matrix.setTextSize(1);
  matrix.setTextColor(1);
  matrix.setCursor(0, 0);
  matrix.print("Cat");
  matrix.setCursor(0, 8);
  matrix.print("Shield");
  matrix.writeScreen();
}

void loop() {
  // if there are incoming bytes available 
  // from the server, read them and print them:
  
}

void clearScreen()
{
  lcd.clear();
  matrix.clearScreen();
}

