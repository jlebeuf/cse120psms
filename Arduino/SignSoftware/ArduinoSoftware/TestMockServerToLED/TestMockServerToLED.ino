
/*
  Web client
 
 This sketch connects to a website (http://www.google.com)
 using a WiFi shield.
 
 This example is written for a network using WPA encryption. For 
 WEP or WPA, change the Wifi.begin() call accordingly.
 
 This example is written for a network using WPA encryption. For 
 WEP or WPA, change the Wifi.begin() call accordingly.
 
 Circuit:
 * WiFi shield attached
 
 created 13 July 2010
 by dlf (Metodo2 srl)
 modified 31 May 2012
 by Tom Igoe
 */


#include <SPI.h>
#include <WiFi.h>
#include <Adafruit_CharacterOLED.h>
#include "HT1632.h"
#define READ_BUFFER_SIZE 128

Adafruit_CharacterOLED lcd(OLED_V2, 14, 15, 16, 17, 18, 19, 2);

#define LED_DATA 3
#define LED_WR 5
#define LED_CS 6
#define LED_CS2 8
//HT1632LEDMatrix matrix = HT1632LEDMatrix(LED_DATA, LED_WR, LED_CS);
HT1632LEDMatrix matrix = HT1632LEDMatrix(LED_DATA, LED_WR, LED_CS, LED_CS2);

char ssid[] = "CATSHIELD"; //  your network SSID (name) 
char pass[] = "CATSHIELD";    // your network password
char serverConnectMessage[] = {0x01, 0x00, 0x00, 0x0, 0x01, 0x02, 0x10};
int serverConnectMessageLength = 7;
char readBuffer[READ_BUFFER_SIZE]; // Buffer to hold received bytes until full message read
int readBufferCount = 0; // Number of bytes in buffer

int status = WL_IDLE_STATUS;
// if you don't want to use DNS (and reduce your sketch size)
// use the numeric IP instead of the name for the server:
//char server[] = "catshielddev.ddns.net";
IPAddress server(192,168,0,103);  // IP address on local network of server
int port = 26262; // Port server is listening on

// Initialize the Ethernet client library
// with the IP address and port of the server 
// that you want to connect to (port 80 is default for HTTP):
WiFiClient client;

void setup() {
  //Initialize serial and wait for port to open:
  Serial.begin(9600); 
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  
  Serial.println("Starting LCD");
  lcd.begin(16, 2);
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Starting LED");
  matrix.begin(HT1632_COMMON_16NMOS);
  matrix.fillScreen();
  delay(1000);
  matrix.clearScreen();
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Starting WiFi");
  delay(1000);
  
  // check for the presence of the shield:
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present"); 
    // don't continue:
    while(true);
  } 
  
  // attempt to connect to Wifi network:
  while ( status != WL_CONNECTED) { 
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Connecting to");
    lcd.setCursor(0, 1);
    lcd.print(ssid);
    delay(1000);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:    
    status = WiFi.begin(ssid, pass);
  
    // poll connection status every half second:
    delay(500);
  }
  Serial.println("Connected to wifi");
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Connected");
  printWifiStatus();
  delay(1000);
  
  Serial.println("\nStarting connection to server...");
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Connecting to");
  lcd.setCursor(0, 1);
  lcd.print(server);
  delay(1000);
  
  connectToServer();
}

void loop() {
  // if there are incoming bytes available 
  // from the server, read them and print them:
  while (true)
  {
    if (!client.connected())
      connectToServer();
    
    while (client.available()) {
      if (readBufferCount >= READ_BUFFER_SIZE)
      {
        Serial.println("Read buffer overflow!");
      } else
      {
        char c = client.read();
        readBuffer[readBufferCount++] = c;
      }
      //Serial.print("Read buffer pre and post:  ");
      //printReadBuffer();
      processReadBuffer();
      //printReadBuffer();
    }
  }
}


void connectToServer() {
  while (!client.connected())
  {
    Serial.println("Connecting to server");
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Connecting to");
    lcd.setCursor(0, 1);
    lcd.print(server);
    client.stop();
    client.connect(server, port);
    delay(5000);
    if (client.connected())
    {
      lcd.clear();
      lcd.setCursor(0, 0);
      lcd.print("Connected");
      
      for (int i = 0; i < serverConnectMessageLength; i++)
        client.write(serverConnectMessage[i]);
      
      delay(1000);
      lcd.clear();
      readBufferCount = 0;
      return;
    }
  }
}

void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

void clearScreen()
{
  lcd.clear();
  matrix.clearScreen();
}

void processReadBuffer()
{
  if (readBufferCount < 2)
    return;

  if (readBuffer[0] == 0x01)
  {
    clearScreen();
    for (int i = 0; i < readBufferCount - 2; i++)
      readBuffer[i] = readBuffer[i + 2];
    readBufferCount -= 2;
  }
  else
  if (readBuffer[0] == 0x07)
  {
    if ((readBufferCount - 3 - readBuffer[1]) >= 0)
    {
      lcd.clear();
      matrix.clearScreen();
      matrix.setTextSize(1);
      matrix.setTextColor(1);
      lcd.setCursor(0, 0);
      matrix.setCursor(0, 0);
      int nextLineNumber = 1;
      int col = 0;
      for (int i = 2; i < readBuffer[1]+2; i++)
      {
        if (readBuffer[i] == 0x00)
        {
          lcd.setCursor(0, nextLineNumber);
          matrix.setCursor(0, nextLineNumber*8);
          nextLineNumber++;
          col = 0;
        }
        else
        {
          lcd.print(readBuffer[i]);
          if (col < matrix.width())
            matrix.print(readBuffer[i]);
          col++;
        }
      }
      matrix.writeScreen();
      int commandSize = 3 + readBuffer[1];
      for (int i = 0; i < readBufferCount-commandSize; i++)
        readBuffer[i] = readBuffer[i + commandSize];
      readBufferCount -= commandSize;
    }
  }
  else
  if (readBuffer[0] == 0x11)
  {
    for (int i = 0; i < serverConnectMessageLength; i++)
        client.write(serverConnectMessage[i]);
    for (int i = 0; i < readBufferCount - 2; i++)
      readBuffer[i] = readBuffer[i + 2];
    readBufferCount -= 2;
  }
}

void printReadBuffer()
{
  /*lcd.clear();
  lcd.setCursor(0, 0);
  int lcdNextLine = 1;
  for (int i = 0; i < readBufferCount; i++)
  {
    Serial.print(readBuffer[i], HEX);
    Serial.print(" ");
    if (readBuffer[i] == '`')
      lcd.setCursor(0, lcdNextLine++);
    else
      lcd.print(readBuffer[i]);
  }//*/
  Serial.println();
  for (int i = 0; i < readBufferCount; i++)
    Serial.print(readBuffer[i]);
  Serial.println();
  
  //readBufferCount = 0;
}

