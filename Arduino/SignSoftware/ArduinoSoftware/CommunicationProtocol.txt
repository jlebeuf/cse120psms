==============================
Connection flow:
==============================
Server listens for connections
Arduino connects and registers self
Server sends commands to Arduino





==============================
Server to Arduino commands:
==============================
General command format:
	[Command #][0x00]
Or:
	[Command #][1 byte Message length = N][N byte Message][0x00]


Flush stream:
	[0x00][0x00][0x00][0x00][0x00][0x00][0x00][0x00][0x00][0x00]
	(10 null bytes (0x00))
	(next bytes ignored until non null received)

Clear screen:
	[0x01][0x00]

Display message:
	[0x07][1 byte message length = N][N byte Message][0x00]
	A null byte (0x00) within message indicates new line

Request sign parameters again (lines/columns):
	[0x11][0x00]



==============================
Arduino to Server commands
==============================
General command format:
	[Command #][0x00]
Or:
	[Command #][1 byte Message length = N][N byte message][0x00]


Flush stream:
	[0x00][0x00][0x00][0x00][0x00][0x00][0x00][0x00][0x00][0x00]
	(10 null bytes (0x00))
	(next bytes ignored until non null received)

Register self as display:
	[0x01][4 byte serial number][1 byte number of display lines][1 byte number of display columns][0x00]

