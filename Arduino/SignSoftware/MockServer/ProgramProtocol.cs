﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace MockCatShieldSignServer
{
    class Program
    {
        static readonly string killCommand = "QUIT";
        static readonly TimeSpan failSleepTimespan = new TimeSpan(0, 0, 10);

        static void Main(string[] args)
        {
            Console.WriteLine("Mock CatShield Sign Server");
            Console.WriteLine("\n\n");

            listenerThreadMethod();
        }

        static void listenerThreadMethod()
        {
            int port = 26262;
            TcpListener listener = null;
            bool connected = false;
            while (!connected)
            {
                Console.WriteLine("Creating TcpListener");
                
                try
                {
                    listener = new TcpListener(IPAddress.Any, port);
                    listener.Start();
                    connected = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed to create TcpListener on port {0}", port);
                    Console.WriteLine("Error: {0}", e.Message);
                    Console.WriteLine("Waiting for {0} seconds before trying again", failSleepTimespan.Seconds);
                    Thread.Sleep(failSleepTimespan);
                }
            }

            Console.WriteLine("TcpListener created on port {0}", port);

            while (true)
            {
                Console.WriteLine("\n\nWaiting for connection");
                TcpClient tcpClient = listener.AcceptTcpClient();
                Console.WriteLine("Connection accepted, handling it");
                handleConnection(tcpClient);
            }
        }

        static void keepAlive(TcpClient tcpClient)
        {
            lock(tcpClient)
            {
                try
                {
                    tcpClient.GetStream().WriteByte(0x0);
                } catch (Exception e)
                {

                }
            }
        }

        static void handleConnection(TcpClient tcpClient)
        {

            byte[] tmp = new byte[1];
            bool peek;
            NetworkStream stream;
            stream = tcpClient.GetStream();
            /*lock (tcpClient)
            {
                stream = tcpClient.GetStream();
                if (tcpClient.Client.Receive(tmp, SocketFlags.Peek) != 0)
                    peek = true;
                else
                    peek = false;
            }//*/
            int clientSerial = 0;
            int clientLines = 0;
            int clientColumns = 0;
            for (int i = 0; i < 7; i++)
            {
                int j = -1;
                try
                {
                    while (j < 0)
                        j = stream.ReadByte();
                } catch (Exception e)
                {

                }

                if (i == 0)
                    continue; // first command byte
                if (i < 5)
                    clientSerial = clientSerial ^ (j << 8*(4 - i));
                else
                if (i == 5)
                    clientLines = j;
                else
                    clientColumns = j;
            }


            while (tcpClient.Connected)// && peek)
            {
                Console.WriteLine("Client [{0:X}]: {1} lines by {2} columns", clientSerial, clientLines, clientColumns);
                Console.WriteLine("c - Close connection");
                Console.WriteLine("1 - Clear screen");
                Console.WriteLine("2 - Send message");
                Console.WriteLine("3 - Request parameters");
                Console.Write("Message type: ");
                string line = Console.ReadLine();

                
                byte[] bytesToSend;

                if (line == "c")
                {
                    Console.WriteLine("Closing connection");
                    tcpClient.Client.Shutdown(SocketShutdown.Both);
                    return;
                }
                else
                if (line == "1")
                {
                    // [0x01][0x00]
                    
                    bytesToSend = new byte[2];
                    bytesToSend[0] = 0x01;
                    bytesToSend[1] = 0x00;
                }
                else
                if (line == "2")
                {
                    // [0x07][1 byte message length = N][N byte Message][0x00]
                    // A null byte (0x00) within message indicates new line
                    
                    Console.WriteLine("Message ('\' for new line, '\\' for '\' without new line):");
                    string message = Console.ReadLine();
                    byte messageLength = (byte)message.Length;
                    byte[] msgASCIIBytes = ASCIIEncoding.ASCII.GetBytes(message);
                    byte[] msgBytes = new byte[messageLength];
                    int msgBytesLength = 0;
                    for (int i = 0; i < messageLength; i++)
                    {
                        if (message[i] == '\\')
                        {
                            if (i + 1 < messageLength && message[i + 1] == '\\')
                            {
                                ASCIIEncoding.ASCII.GetBytes(message, i, 1, msgBytes, msgBytesLength);
                                i++; // Two characters making one character ('\\' -> '\')
                            }
                            else
                            {
                                // '\' -> null byte (0x00)
                                msgBytes[msgBytesLength] = 0x00;
                            }
                        } else
                        {
                            ASCIIEncoding.ASCII.GetBytes(message, i, 1, msgBytes, msgBytesLength);
                        }
                        msgBytesLength++;
                    }

                    bytesToSend = new byte[3 + msgBytesLength];
                    bytesToSend[0] = 0x07;
                    bytesToSend[1] = (byte)msgBytesLength;
                    for (int i = 0; i < msgBytesLength; i++)
                        bytesToSend[2 + i] = msgBytes[i];
                    bytesToSend[bytesToSend.Length - 1] = 0x00;
                }
                else
                if (line == "3")
                {
                    // [0x11][0x00]

                    bytesToSend = new byte[2];
                    bytesToSend[0] = 0x11;
                    bytesToSend[1] = 0x00;
                }
                else
                {
                    Console.WriteLine("Invalid type");
                    continue;
                }


                for (byte i = 0; i < bytesToSend.Length; i++)
                    Console.Write("{0:X} ", bytesToSend[i]);
                Console.WriteLine();

                try
                {
                    stream.Write(bytesToSend, 0, bytesToSend.Length);
                } catch (Exception e)
                {
                    Console.WriteLine("Failed to write to client, disconnected?");
                }
            }
        }
    }
}
