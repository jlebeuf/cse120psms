
/*
  Web client
 
 This sketch connects to a website (http://www.google.com)
 using a WiFi shield.
 
 This example is written for a network using WPA encryption. For 
 WEP or WPA, change the Wifi.begin() call accordingly.
 
 This example is written for a network using WPA encryption. For 
 WEP or WPA, change the Wifi.begin() call accordingly.
 
 Circuit:
 * WiFi shield attached
 
 created 13 July 2010
 by dlf (Metodo2 srl)
 modified 31 May 2012
 by Tom Igoe
 */


#include <SPI.h>
#include <WiFi.h>
#define READ_BUFFER_SIZE 128

char ssid[] = "CATSHIELD"; //  your network SSID (name) 
char pass[] = "CATSHIELD";    // your network password
char readBuffer[READ_BUFFER_SIZE]; // Buffer to hold received bytes until full message read
int readBufferCount = 0; // Number of bytes in buffer

int status = WL_IDLE_STATUS;
// if you don't want to use DNS (and reduce your sketch size)
// use the numeric IP instead of the name for the server:
IPAddress server(192,168,0,100);  // IP address on local network of server
int port = 26262; // Port server is listening on

// Initialize the Ethernet client library
// with the IP address and port of the server 
// that you want to connect to (port 80 is default for HTTP):
WiFiClient client;

void setup() {
  //Initialize serial and wait for port to open:
  Serial.begin(9600); 
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }
  
  // check for the presence of the shield:
  if (WiFi.status() == WL_NO_SHIELD) {
    Serial.println("WiFi shield not present"); 
    // don't continue:
    while(true);
  } 
  
  // attempt to connect to Wifi network:
  while ( status != WL_CONNECTED) { 
    Serial.print("Attempting to connect to SSID: ");
    Serial.println(ssid);
    // Connect to WPA/WPA2 network. Change this line if using open or WEP network:    
    status = WiFi.begin(ssid, pass);
  
    // poll connection status every half second:
    delay(500);
  } 
  Serial.println("Connected to wifi");
  printWifiStatus();
  
  Serial.println("\nStarting connection to server...");
  // if you get a connection, report back via serial:
  if (client.connect(server, port)) {
    Serial.println("connected to server");
  }
}

void loop() {
  // if there are incoming bytes available 
  // from the server, read them and print them:
  while (true)
  {
    while (!client.connected())
    {
      Serial.println("Lost connection, reconnecting");
      client.stop();
      client.connect(server, port);
      delay(5000);
    }
    while (client.available()) {
      if (readBufferCount >= READ_BUFFER_SIZE)
      {
        Serial.println("Read buffer overflow!");
      } else
      {
        char c = client.read();
        if (c == 0x0)
          printReadBuffer();
        else
          readBuffer[readBufferCount++] = c;
      }
    }
    delay(1000);
    // Keep connection alive with null byte
    client.write("\0");
  }
  

  // if the server's disconnected, stop the client:
  if (!client.connected()) {
    Serial.println();
    Serial.println("disconnecting from server.");
    client.stop();

    // do nothing forevermore:
    while(true);
  }
}


void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

void printReadBuffer()
{
  for (int i = 0; i < readBufferCount; i++)
  {
    Serial.print(readBuffer[i], HEX);
    Serial.print(" ");
  }
  Serial.println();
  for (int i = 0; i < readBufferCount; i++)
    Serial.print(readBuffer[i]);
  Serial.println();
  
  readBufferCount = 0;
}





