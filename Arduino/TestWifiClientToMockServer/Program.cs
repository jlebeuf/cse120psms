﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace MockCatShieldSignServer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Mock CatShield Sign Server");
            Console.WriteLine("Press any key at any time to exit");
            Console.WriteLine("\n\n");

            listenerThreadMethod();

            /*Thread listenerThread = new Thread(new ThreadStart(listenerThreadMethod));
            listenerThread.IsBackground = true;
            listenerThread.Start();//*/

            Console.ReadKey();
        }

        static void listenerThreadMethod()
        {
            /*while (true)
            {
                Console.WriteLine("Waiting for connection");
                TimeSpan ts = new TimeSpan(0, 0, 1);
                for (int i = 0; i < 5; i++)
                {
                    Thread.Sleep(ts);
                    Console.Write(". ");
                }
                Console.WriteLine();
                //handleConnection();
                Console.WriteLine("Connection handled");
            }//*/

            int port = 26262;
            TcpListener listener = null;
            bool connected = false;
            TimeSpan failSleepTimespan = new TimeSpan(0, 0, 30);
            while (!connected)
            {
                Console.WriteLine("Creating TcpListener");
                
                try
                {
                    listener = new TcpListener(IPAddress.Any, port);
                    listener.Start();
                    connected = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine("Failed to create TcpListener on port {0}", port);
                    Console.WriteLine("Error: {0}", e.Message);
                    Console.WriteLine("Waiting for {0} seconds before trying again", failSleepTimespan.Seconds);
                    Thread.Sleep(failSleepTimespan);
                }
            }

            Console.WriteLine("TcpListener created, waiting for TCP connection on port {0}", port);

            while (true)
            {
                TcpClient tcpClient = listener.AcceptTcpClient();
                Console.WriteLine("\n\nConnection accepted, handling it");
                handleConnection(tcpClient);
            }
        }

        static void keepAlive(TcpClient tcpClient)
        {
            lock(tcpClient)
            {
                try
                {
                    tcpClient.GetStream().WriteByte(0x0);
                } catch (Exception e)
                {

                }
            }
        }

        static void handleConnection(TcpClient tcpClient)
        {

            byte[] tmp = new byte[1];
            bool peek;
            NetworkStream stream;
            stream = tcpClient.GetStream();
            /*lock (tcpClient)
            {
                stream = tcpClient.GetStream();
                if (tcpClient.Client.Receive(tmp, SocketFlags.Peek) != 0)
                    peek = true;
                else
                    peek = false;
            }//*/

            while (tcpClient.Connected)// && peek)
            {
                Console.Write("Enter message to send: ");
                string str = Console.ReadLine();

                byte[] strBytes = ASCIIEncoding.ASCII.GetBytes(str);
                Console.WriteLine("\nWriting bytes to client:");
                byte[] bytes = new byte[strBytes.Length + 1];
                for (byte i = 0; i < strBytes.Length; i++)
                {
                    bytes[i] = strBytes[i];
                    Console.Write("{0:X} ", bytes[i]);
                }
                Console.WriteLine();
                bytes[strBytes.Length] = 0x00;
                try {
                    //lock (tcpClient)
                        stream.Write(bytes, 0, bytes.Length);
                } catch (Exception e) {
                    Console.WriteLine("Failed to write to client, disconnected?");
                }
            }
        }
    }
}
